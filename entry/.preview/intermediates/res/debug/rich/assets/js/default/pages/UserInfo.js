/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为导航栏，除了特定页面如闪屏页面，都应该有导航栏。
 * 导航栏固定高度60vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.navigationBar = void 0;
var router = globalThis.requireNativeModule('system.router');
class navigationBar extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__title = new ObservedPropertySimple('', this, "title");
        this.__isBack = new ObservedPropertySimple(true, this, "isBack");
        this.__isMenu = new ObservedPropertySimple(true, this, "isMenu");
        this.__isSearch = new ObservedPropertySimple(true, this, "isSearch");
        this.__isMore = new ObservedPropertySimple(false, this, "isMore");
        this.__isClose = new ObservedPropertySimple(false, this, "isClose");
        this.__bgColor = new ObservedPropertyObject({ "id": 16777285, "type": 10001, params: [] }, this, "bgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.title !== undefined) {
            this.title = params.title;
        }
        if (params.isBack !== undefined) {
            this.isBack = params.isBack;
        }
        if (params.isMenu !== undefined) {
            this.isMenu = params.isMenu;
        }
        if (params.isSearch !== undefined) {
            this.isSearch = params.isSearch;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isClose !== undefined) {
            this.isClose = params.isClose;
        }
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
    }
    aboutToBeDeleted() {
        this.__title.aboutToBeDeleted();
        this.__isBack.aboutToBeDeleted();
        this.__isMenu.aboutToBeDeleted();
        this.__isSearch.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isClose.aboutToBeDeleted();
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    get isBack() {
        return this.__isBack.get();
    }
    set isBack(newValue) {
        this.__isBack.set(newValue);
    }
    get isMenu() {
        return this.__isMenu.get();
    }
    set isMenu(newValue) {
        this.__isMenu.set(newValue);
    }
    get isSearch() {
        return this.__isSearch.get();
    }
    set isSearch(newValue) {
        this.__isSearch.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isClose() {
        return this.__isClose.get();
    }
    set isClose(newValue) {
        this.__isClose.set(newValue);
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/navigationBar.ets(21:4)");
        Row.height(60);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(26:7)");
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isBack == true) {
            If.branchId(0);
            Image.create({ "id": 16777296, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(28:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        If.create();
        if (this.isClose == true) {
            If.branchId(0);
            Image.create({ "id": 16777299, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(36:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.pop();
        /**
         * 中间标题栏
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(47:7)");
        /**
         * 中间标题栏
         */
        Column.layoutWeight(1);
        /**
         * 中间标题栏
         */
        Column.padding({ left: 20 });
        Text.create(this.title);
        Text.debugLine("common/components/navigationBar.ets(48:9)");
        Text.fontSize(18);
        Text.fontFamily({ "id": 117440975, "type": 10002, params: [] });
        Text.maxLines(1);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.pop();
        /**
         * 中间标题栏
         */
        Column.pop();
        /**
         * 右侧搜索按钮
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(57:7)");
        /**
         * 右侧搜索按钮
         */
        Column.width(30);
        If.create();
        if (this.isSearch == true) {
            If.branchId(0);
            Image.create({ "id": 16777328, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(59:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧搜索按钮
         */
        Column.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(68:7)");
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isMenu == true) {
            If.branchId(0);
            Image.create({ "id": 16777295, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(70:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        If.create();
        if (this.isMore == true) {
            If.branchId(0);
            Image.create({ "id": 16777322, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(75:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.pop();
        Row.pop();
    }
}
exports.navigationBar = navigationBar;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets":
/*!****************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为设置和其他类似UI的选项布局。
 * 主要内容为图片+文字+导航箭头
 * powered by yiyefangzhou24
 * 2022/5/15
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.optionItem = void 0;
class optionItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__bgColor = new ObservedPropertyObject({ "id": 16777290, "type": 10001, params: [] }, this, "bgColor");
        this.img = undefined;
        this.context = undefined;
        this.isNext = undefined;
        this.marginTop = undefined;
        this.itemOnClick = (event) => { } //item的单击事件（可选）
        ;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
        if (params.img !== undefined) {
            this.img = params.img;
        }
        if (params.context !== undefined) {
            this.context = params.context;
        }
        if (params.isNext !== undefined) {
            this.isNext = params.isNext;
        }
        if (params.marginTop !== undefined) {
            this.marginTop = params.marginTop;
        }
        if (params.itemOnClick !== undefined) {
            this.itemOnClick = params.itemOnClick;
        }
    }
    aboutToBeDeleted() {
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(19:5)");
        Row.width('100%');
        Row.height(60);
        Row.margin({ top: this.marginTop });
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        Row.onTouch((event) => {
            if (event.type === TouchType.Down) {
                this.bgColor = { "id": 16777287, "type": 10001, params: [] };
            }
            if (event.type === TouchType.Up) {
                this.bgColor = { "id": 16777290, "type": 10001, params: [] };
            }
        });
        Row.onClick((event) => {
            this.itemOnClick(event);
        });
        If.create();
        if (this.img != null) {
            If.branchId(0);
            Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center });
            Flex.debugLine("common/utils/optionItem.ets(21:9)");
            Flex.width(50);
            Flex.height(60);
            Image.create(this.img);
            Image.debugLine("common/utils/optionItem.ets(22:11)");
            Image.width(30);
            Image.height(30);
            Flex.pop();
        }
        If.pop();
        Column.create();
        Column.debugLine("common/utils/optionItem.ets(26:7)");
        Column.padding({ left: 10, right: 4 });
        Column.height(60);
        Column.layoutWeight(1);
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(27:9)");
        Row.height(59);
        Text.create(this.context);
        Text.debugLine("common/utils/optionItem.ets(28:11)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(19);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.layoutWeight(1);
        Text.pop();
        If.create();
        //.margin({left: 10})
        if (this.isNext == true) {
            If.branchId(0);
            Image.create({ "id": 16777324, "type": 20000, params: [] });
            Image.debugLine("common/utils/optionItem.ets(36:13)");
            Image.width(18);
            Image.height(18);
            Image.margin(10);
        }
        If.pop();
        Row.pop();
        Divider.create();
        Divider.debugLine("common/utils/optionItem.ets(42:9)");
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        Divider.strokeWidth(0.8);
        Column.pop();
        Row.pop();
    }
}
exports.optionItem = optionItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets":
/*!***************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 联系人数据结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ContactData = exports.Gender = void 0;
var Gender;
(function (Gender) {
    //女性
    Gender[Gender["female"] = 0] = "female";
    //男性
    Gender[Gender["male"] = 1] = "male";
})(Gender = exports.Gender || (exports.Gender = {}));
class ContactData {
    constructor(wid, headImg, name, mark, gender, area) {
        this.wid = wid;
        this.headImg = headImg;
        this.name = name;
        this.mark = mark;
        this.gender = gender;
        this.area = area;
    }
}
exports.ContactData = ContactData;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!*************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/UserInfo.ets?entry ***!
  \*************************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为用户信息页面。
 * powered by yiyefangzhou24
 * 2022/5/16
 */
var router = globalThis.requireNativeModule('system.router');
const navigationBar_ets_1 = __webpack_require__(/*! ../common/components/navigationBar.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets");
const ContactData_ets_1 = __webpack_require__(/*! ../model/data/ContactData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets");
const optionItem_ets_1 = __webpack_require__(/*! ../common/utils/optionItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets");
class UserInfo extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.contactData = router.getParams().contactData //用户信息
        ;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.contactData !== undefined) {
            this.contactData = params.contactData;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    //private contactData: ContactData = new ContactData("10000" , "header1.png" , "刘备" , "刘备" , Gender.male , "三国·蜀国")
    render() {
        Column.create();
        Column.debugLine("pages/UserInfo.ets(17:5)");
        Column.width('100%');
        Column.height('100%');
        Column.backgroundColor({ "id": 16777287, "type": 10001, params: [] });
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            /**
             * 顶部导航栏
             */
            View.create(new navigationBar_ets_1.navigationBar("2", this, { isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: true, bgColor: { "id": 16777290, "type": 10001, params: [] } }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: true,
                bgColor: { "id": 16777290, "type": 10001, params: [] }
            });
            View.create(earlierCreatedChild_2);
        }
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.create();
        Row.debugLine("pages/UserInfo.ets(27:7)");
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.width('100%');
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.height(160);
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.padding({ left: 20, right: 20 });
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.alignItems(VerticalAlign.Center);
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Image.create($rawfile(this.contactData.headImg));
        Image.debugLine("pages/UserInfo.ets(28:9)");
        Image.width(70);
        Image.height(70);
        Image.borderRadius(8);
        Flex.create({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Start });
        Flex.debugLine("pages/UserInfo.ets(31:9)");
        Flex.height(160);
        Flex.layoutWeight(1);
        Flex.padding(20);
        Row.create();
        Row.debugLine("pages/UserInfo.ets(32:11)");
        Text.create(this.contactData.name);
        Text.debugLine("pages/UserInfo.ets(33:13)");
        Text.fontFamily({ "id": 117440974, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777281, "type": 10001, params: [] });
        Text.fontSize(22);
        Text.maxLines(1);
        Text.fontWeight(FontWeight.Bold);
        Text.pop();
        If.create();
        if (this.contactData.gender == ContactData_ets_1.Gender.female) {
            If.branchId(0);
            Image.create({ "id": 16777301, "type": 20000, params: [] });
            Image.debugLine("pages/UserInfo.ets(40:15)");
            Image.height(18);
            Image.width(18);
            Image.margin({ left: 8 });
        }
        else if (this.contactData.gender == ContactData_ets_1.Gender.male) {
            If.branchId(1);
            Image.create({ "id": 16777313, "type": 20000, params: [] });
            Image.debugLine("pages/UserInfo.ets(42:15)");
            Image.height(18);
            Image.width(18);
            Image.margin({ left: 8 });
        }
        If.pop();
        Row.pop();
        Row.create();
        Row.debugLine("pages/UserInfo.ets(45:11)");
        Row.margin({ top: 6 });
        Text.create({ "id": 16777235, "type": 10003, params: [] });
        Text.debugLine("pages/UserInfo.ets(46:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontSize(14);
        Text.maxLines(1);
        Text.pop();
        Text.create(this.contactData.wid);
        Text.debugLine("pages/UserInfo.ets(51:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontSize(14);
        Text.maxLines(1);
        Text.pop();
        Row.pop();
        Row.create();
        Row.debugLine("pages/UserInfo.ets(57:11)");
        Row.margin({ top: 4 });
        Text.create({ "id": 16777228, "type": 10003, params: [] });
        Text.debugLine("pages/UserInfo.ets(58:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontSize(14);
        Text.maxLines(1);
        Text.pop();
        Text.create(this.contactData.area);
        Text.debugLine("pages/UserInfo.ets(63:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontSize(14);
        Text.maxLines(1);
        Text.pop();
        Row.pop();
        Flex.pop();
        /**
         * 个人信息栏
         * 包括头像/昵称/微信号/地区
         */
        Row.pop();
        //分割线
        Divider.create();
        Divider.debugLine("pages/UserInfo.ets(78:7)");
        //分割线
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        //分割线
        Divider.strokeWidth(0.8);
        //分割线
        Divider.margin({ left: 10, right: 10 });
        /**
         * 选项栏
         * 包括备注/朋友权限
         */
        Column.create();
        Column.debugLine("pages/UserInfo.ets(84:7)");
        let earlierCreatedChild_3 = this.findChildById("3");
        if (earlierCreatedChild_3 == undefined) {
            //设置备注和标签
            View.create(new optionItem_ets_1.optionItem("3", this, { context: { "id": 16777234, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_3.updateWithValueParams({
                context: { "id": 16777234, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_3);
        }
        let earlierCreatedChild_4 = this.findChildById("4");
        if (earlierCreatedChild_4 == undefined) {
            //朋友权限
            View.create(new optionItem_ets_1.optionItem("4", this, { context: { "id": 16777233, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_4.updateWithValueParams({
                context: { "id": 16777233, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_4);
        }
        /**
         * 选项栏
         * 包括备注/朋友权限
         */
        Column.pop();
        /**
         * 朋友圈及更多信息
         */
        Column.create();
        Column.debugLine("pages/UserInfo.ets(94:7)");
        /**
         * 朋友圈及更多信息
         */
        Column.margin({ top: 10 });
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            //朋友圈
            View.create(new optionItem_ets_1.optionItem("5", this, { context: { "id": 16777231, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({
                context: { "id": 16777231, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_5);
        }
        let earlierCreatedChild_6 = this.findChildById("6");
        if (earlierCreatedChild_6 == undefined) {
            //更多信息
            View.create(new optionItem_ets_1.optionItem("6", this, { context: { "id": 16777232, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_6.updateWithValueParams({
                context: { "id": 16777232, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_6);
        }
        /**
         * 朋友圈及更多信息
         */
        Column.pop();
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/UserInfo.ets(106:7)");
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.width('100%');
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.height(60);
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.margin({ top: 10 });
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Image.create({ "id": 16777333, "type": 20000, params: [] });
        Image.debugLine("pages/UserInfo.ets(107:9)");
        Image.height(24);
        Image.width(24);
        Image.margin({ right: 4 });
        Text.create({ "id": 16777229, "type": 10003, params: [] });
        Text.debugLine("pages/UserInfo.ets(108:9)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777282, "type": 10001, params: [] });
        Text.fontSize(19);
        Text.maxLines(1);
        Text.pop();
        /**
         * 发送消息及视频按钮
         * 注：目前Button控件貌似不支持width设置为100%，此处需要顶格，故更换其他组建
         */
        //发消息按钮
        Flex.pop();
        //视频童通话按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/UserInfo.ets(117:7)");
        //视频童通话按钮
        Flex.width('100%');
        //视频童通话按钮
        Flex.height(60);
        //视频童通话按钮
        Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Image.create({ "id": 16777334, "type": 20000, params: [] });
        Image.debugLine("pages/UserInfo.ets(118:9)");
        Image.height(24);
        Image.width(24);
        Image.margin({ right: 4 });
        Text.create({ "id": 16777230, "type": 10003, params: [] });
        Text.debugLine("pages/UserInfo.ets(119:9)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777282, "type": 10001, params: [] });
        Text.fontSize(19);
        Text.maxLines(1);
        Text.pop();
        //视频童通话按钮
        Flex.pop();
        Column.pop();
    }
}
loadDocument(new UserInfo("1", undefined, {}));

})();

/******/ })()
;