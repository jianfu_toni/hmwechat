/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MomentListItem.ets":
/*!**********************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MomentListItem.ets ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 该组件为朋友圈页面好友动态List的一个item布局。
 * MomentList => item
 * powered by yiyefangzhou24
 * 2022/5/16
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MomentListItem = void 0;
const MomentData_ets_1 = __webpack_require__(/*! ../../../model/data/MomentData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MomentData.ets");
class MomentListItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.momentItem = undefined;
        this.__imgHeight = new ObservedPropertySimple(0 //图片高度（用于刷新显示正确的图片比例）
        , this, "imgHeight");
        this.__imgWidth = new ObservedPropertySimple(0 //图片宽度（用于刷新现实正确的图片比例）
        , this, "imgWidth");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.momentItem !== undefined) {
            this.momentItem = params.momentItem;
        }
        if (params.imgHeight !== undefined) {
            this.imgHeight = params.imgHeight;
        }
        if (params.imgWidth !== undefined) {
            this.imgWidth = params.imgWidth;
        }
    }
    aboutToBeDeleted() {
        this.__imgHeight.aboutToBeDeleted();
        this.__imgWidth.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get imgHeight() {
        return this.__imgHeight.get();
    }
    set imgHeight(newValue) {
        this.__imgHeight.set(newValue);
    }
    get imgWidth() {
        return this.__imgWidth.get();
    }
    set imgWidth(newValue) {
        this.__imgWidth.set(newValue);
    }
    render() {
        Column.create();
        Column.debugLine("common/components/listitem/MomentListItem.ets(17:5)");
        Column.width('100%');
        Column.padding({ left: 10, right: 10 });
        //动态内容布局
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Start, alignItems: ItemAlign.Start });
        Flex.debugLine("common/components/listitem/MomentListItem.ets(19:7)");
        //动态内容布局
        Flex.padding({ bottom: 10 });
        //头像
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/MomentListItem.ets(21:9)");
        //头像
        Flex.height(60);
        //头像
        Flex.width(60);
        Image.create($rawfile(this.momentItem.headImg));
        Image.debugLine("common/components/listitem/MomentListItem.ets(22:11)");
        Image.borderRadius(5);
        Image.width(50);
        Image.height(50);
        //头像
        Flex.pop();
        //内容
        Column.create();
        Column.debugLine("common/components/listitem/MomentListItem.ets(27:9)");
        //内容
        Column.padding({ top: 10, left: 10 });
        //内容
        Column.alignItems(HorizontalAlign.Start);
        //内容
        Column.layoutWeight(1);
        //用户名
        Text.create(this.momentItem.name);
        Text.debugLine("common/components/listitem/MomentListItem.ets(29:11)");
        //用户名
        Text.fontColor({ "id": 16777282, "type": 10001, params: [] });
        //用户名
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        //用户名
        Text.fontSize(16);
        //用户名
        Text.margin({ bottom: 2 });
        //用户名
        Text.pop();
        If.create();
        //文字评论
        if (this.momentItem.param1.length > 0) {
            If.branchId(0);
            Text.create(this.momentItem.param1);
            Text.debugLine("common/components/listitem/MomentListItem.ets(36:13)");
            Text.fontColor({ "id": 16777281, "type": 10001, params: [] });
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(16);
            Text.margin({ bottom: 2 });
            Text.pop();
        }
        If.pop();
        If.create();
        //媒体资料
        if (this.momentItem.param2.length > 0) {
            If.branchId(0);
            If.create();
            if (this.momentItem.type == MomentData_ets_1.momentType.Pic) {
                If.branchId(0);
                Image.create($rawfile(this.momentItem.param2));
                Image.debugLine("common/components/listitem/MomentListItem.ets(45:15)");
                Image.sourceSize({ width: this.imgWidth, height: this.imgHeight });
                Image.objectFit(ImageFit.Contain);
                Image.onComplete((msg) => {
                    this.imgWidth = msg.width;
                    this.imgHeight = msg.height;
                });
                Image.margin({ bottom: 2 });
            }
            else if (this.momentItem.type == MomentData_ets_1.momentType.Video) {
                If.branchId(1);
            }
            else if (this.momentItem.type == MomentData_ets_1.momentType.Link) {
                If.branchId(2);
            }
            If.pop();
        }
        If.pop();
        //时间和评论按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/MomentListItem.ets(60:11)");
        //时间和评论按钮
        Flex.height(30);
        Text.create(this.momentItem.time);
        Text.debugLine("common/components/listitem/MomentListItem.ets(61:13)");
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(11);
        Text.pop();
        Column.create();
        Column.debugLine("common/components/listitem/MomentListItem.ets(65:13)");
        Column.height(20);
        Column.width(30);
        Column.alignItems(HorizontalAlign.Center);
        Column.borderRadius(4);
        Column.backgroundColor({ "id": 16777286, "type": 10001, params: [] });
        Image.create({ "id": 16777320, "type": 20000, params: [] });
        Image.debugLine("common/components/listitem/MomentListItem.ets(66:15)");
        Image.height(20);
        Image.width(20);
        Column.pop();
        //时间和评论按钮
        Flex.pop();
        If.create();
        //评论布局
        if (this.momentItem.comments.length > 0) {
            If.branchId(0);
            Flex.create();
            Flex.debugLine("common/components/listitem/MomentListItem.ets(76:13)");
            Column.create();
            Column.debugLine("common/components/listitem/MomentListItem.ets(77:15)");
            Column.backgroundColor({ "id": 16777286, "type": 10001, params: [] });
            Column.padding(6);
            Column.borderRadius(4);
            Column.margin({ top: 6 });
            List.create({ space: 3 });
            List.debugLine("common/components/listitem/MomentListItem.ets(78:17)");
            ForEach.create("1", this, ObservedObject.GetRawObject(this.momentItem.comments), item => {
                ListItem.create();
                ListItem.debugLine("common/components/listitem/MomentListItem.ets(80:21)");
                Text.create();
                Text.debugLine("common/components/listitem/MomentListItem.ets(81:23)");
                Span.create(item.name + '：');
                Span.debugLine("common/components/listitem/MomentListItem.ets(82:25)");
                Span.fontColor({ "id": 16777282, "type": 10001, params: [] });
                Span.fontFamily({ "id": 117440980, "type": 10002, params: [] });
                Span.fontSize(12);
                Span.create(item.context);
                Span.debugLine("common/components/listitem/MomentListItem.ets(86:25)");
                Span.fontFamily({ "id": 117440980, "type": 10002, params: [] });
                Span.fontSize(12);
                Text.pop();
                ListItem.pop();
            });
            ForEach.pop();
            List.pop();
            Column.pop();
            Flex.pop();
        }
        If.pop();
        //内容
        Column.pop();
        //动态内容布局
        Flex.pop();
        //底部分割线
        Divider.create();
        Divider.debugLine("common/components/listitem/MomentListItem.ets(99:7)");
        //底部分割线
        Divider.width('90%');
        //底部分割线
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        //底部分割线
        Divider.strokeWidth(0.8);
        Column.pop();
    }
}
exports.MomentListItem = MomentListItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MomentModel.ets":
/*!**********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MomentModel.ets ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 用于初始化MomentData的数组
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getMomentDatas = void 0;
const MomentData_ets_1 = __webpack_require__(/*! ./data/MomentData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MomentData.ets");
function getMomentDatas() {
    let momentDataArray = [];
    MomentsComposition.forEach(momentitem => {
        //构造评论结构数组
        let commentDataArray = [];
        momentitem.comments.forEach(commentitem => {
            commentDataArray.push(new MomentData_ets_1.CommentData(commentitem.wid, commentitem.name, commentitem.context));
        });
        momentDataArray.push(new MomentData_ets_1.MomentData(momentitem.wid, momentitem.name, momentitem.headImg, momentitem.type, momentitem.param1, momentitem.param2, momentitem.time, commentDataArray));
    });
    momentDataArray.forEach(item => {
        console.log('wid:' + item.wid);
    });
    return momentDataArray;
}
exports.getMomentDatas = getMomentDatas;
/**
 * 定义emoji数据测试样例
 */
const MomentsComposition = [
    {
        "wid": "10001",
        "name": "诸葛亮",
        "headImg": "header2.png",
        "type": MomentData_ets_1.momentType.Text,
        "param1": "今南方已定，兵甲已足，当奖率三军，北定中原，庶竭驽钝，攘除奸凶，兴复汉室，还于旧都。此臣所以报先帝而忠陛下之职分也。",
        "param2": "",
        "time": "1个小时以前",
        "comments": [
            {
                "wid": "10002",
                "name": "张飞",
                "context": "燕人张翼德在此,谁敢来决死战?"
            },
            {
                "wid": "10000",
                "name": "刘备",
                "context": "汉贼不两立，王业不偏安"
            }
        ]
    },
    {
        "wid": "10002",
        "name": "关羽",
        "headImg": "header3.png",
        "type": MomentData_ets_1.momentType.Pic,
        "param1": "酒且斟下，某去便来",
        "param2": "sample1.jpg",
        "time": "1天前",
        "comments": []
    },
    {
        "wid": "10000",
        "name": "刘备",
        "headImg": "header1.png",
        "type": MomentData_ets_1.momentType.Pic,
        "param1": "",
        "param2": "sample_picture.jpeg",
        "time": "2天前",
        "comments": [
            {
                "wid": "10002",
                "name": "张飞",
                "context": "燕人张翼德在此,谁敢来决死战?"
            },
            {
                "wid": "10000",
                "name": "刘备",
                "context": "汉贼不两立，王业不偏安"
            }
        ]
    }
];


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MomentData.ets":
/*!**************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MomentData.ets ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 朋友圈页面动态数据条目结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MomentData = exports.CommentData = exports.momentType = void 0;
/**
 * 动态的类型
 * 主要有：文字/链接/图片/视频
 */
var momentType;
(function (momentType) {
    //文字
    momentType[momentType["Text"] = 0] = "Text";
    //链接
    momentType[momentType["Link"] = 1] = "Link";
    //图片
    momentType[momentType["Pic"] = 2] = "Pic";
    //视频
    momentType[momentType["Video"] = 3] = "Video";
})(momentType = exports.momentType || (exports.momentType = {}));
/**
 * 动态下评论的数据结构
 * @param wid 用户wid
 * @param name  用户名
 * @param context 评论内容
 * @param io  发送/接收
 */
class CommentData {
    constructor(wid, name, context) {
        this.wid = wid;
        this.name = name;
        this.context = context;
    }
}
exports.CommentData = CommentData;
/**
 * 动态内容数据结构
 * @param wid       用户wid
 * @param name      用户名
 * @param headImg   用户头像
 * @param type      动态类型
 * @param io        发送/接收
 * @param param1    参数1（一般存放文字内容）
 * @param param2    参数2（一般存放图片、视频、链接等内容）
 * @param comments  评论内容结构数组
 */
class MomentData {
    constructor(wid, name, headImg, type, param1, param2, time, comments) {
        this.wid = wid;
        this.name = name;
        this.headImg = headImg;
        this.type = type;
        this.param1 = param1;
        this.param2 = param2;
        this.time = time;
        this.comments = comments;
    }
}
exports.MomentData = MomentData;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Moments.ets?entry ***!
  \************************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为朋友圈页面。
 * powered by yiyefangzhou24
 * 2022/5/14
 */
__webpack_require__(/*! ../model/data/MomentData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MomentData.ets");
const MomentModel_ets_1 = __webpack_require__(/*! ../model/MomentModel.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MomentModel.ets");
const MomentListItem_ets_1 = __webpack_require__(/*! ../common/components/listitem/MomentListItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MomentListItem.ets");
var router = globalThis.requireNativeModule('system.router');
class Moments extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.name = '刘备' //用户昵称
        ;
        this.momentItems = MomentModel_ets_1.getMomentDatas() //获取好友动态数据
        ;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.name !== undefined) {
            this.name = params.name;
        }
        if (params.momentItems !== undefined) {
            this.momentItems = params.momentItems;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Column.create();
        Column.debugLine("pages/Moments.ets(18:5)");
        /**
         * 顶部封面布局
         */
        Stack.create({ alignContent: Alignment.Top });
        Stack.debugLine("pages/Moments.ets(22:7)");
        /**
         * 顶部封面布局
         */
        Stack.width('100%');
        /**
         * 顶部封面布局
         */
        Stack.height(280);
        //最下层的封面图片
        Image.create({ "id": 0, "type": 30000, params: ['discovery_bg.jpg'] });
        Image.debugLine("pages/Moments.ets(24:9)");
        //最下层的封面图片
        Image.objectFit(ImageFit.Cover);
        //最下层的封面图片
        Image.height(250);
        //最下层的封面图片
        Image.width('100%');
        //返回按钮和拍摄按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/Moments.ets(29:9)");
        //返回按钮和拍摄按钮
        Flex.width('100%');
        //返回按钮和拍摄按钮
        Flex.height(60);
        //返回按钮和拍摄按钮
        Flex.zIndex(1);
        Image.create({ "id": 16777297, "type": 20000, params: [] });
        Image.debugLine("pages/Moments.ets(30:11)");
        Image.height(30);
        Image.width(30);
        Image.margin({ left: 15 });
        Image.onClick(() => {
            router.back();
        });
        Image.create({ "id": 16777298, "type": 20000, params: [] });
        Image.debugLine("pages/Moments.ets(37:11)");
        Image.height(30);
        Image.width(30);
        Image.margin({ right: 15 });
        //返回按钮和拍摄按钮
        Flex.pop();
        //头像
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.End, alignItems: ItemAlign.End });
        Flex.debugLine("pages/Moments.ets(43:9)");
        //头像
        Flex.width('100%');
        //头像
        Flex.height(280);
        Text.create(this.name);
        Text.debugLine("pages/Moments.ets(44:11)");
        Text.margin({ bottom: 35, right: 10 });
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777290, "type": 10001, params: [] });
        Text.fontSize(18);
        Text.maxLines(1);
        Text.pop();
        Image.create({ "id": 0, "type": 30000, params: ['header1.png'] });
        Image.debugLine("pages/Moments.ets(50:11)");
        Image.width(75);
        Image.height(75);
        Image.borderRadius(8);
        Image.margin({ right: 15 });
        //头像
        Flex.pop();
        /**
         * 顶部封面布局
         */
        Stack.pop();
        /**
         * 下面的动态列表
         */
        List.create();
        List.debugLine("pages/Moments.ets(59:7)");
        /**
         * 下面的动态列表
         */
        List.width('100%');
        /**
         * 下面的动态列表
         */
        List.layoutWeight(1);
        ForEach.create("3", this, ObservedObject.GetRawObject(this.momentItems), item => {
            ListItem.create();
            ListItem.debugLine("pages/Moments.ets(61:11)");
            let earlierCreatedChild_2 = this.findChildById("2");
            if (earlierCreatedChild_2 == undefined) {
                View.create(new MomentListItem_ets_1.MomentListItem("2", this, { momentItem: item }));
            }
            else {
                earlierCreatedChild_2.updateWithValueParams({
                    momentItem: item
                });
                View.create(earlierCreatedChild_2);
            }
            ListItem.pop();
        });
        ForEach.pop();
        /**
         * 下面的动态列表
         */
        List.pop();
        Column.pop();
    }
}
loadDocument(new Moments("1", undefined, {}));

})();

/******/ })()
;