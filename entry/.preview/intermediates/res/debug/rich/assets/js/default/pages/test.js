/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it uses a non-standard name for the exports (exports).
(() => {
var exports = __webpack_exports__;
/*!*********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/test.ets?entry ***!
  \*********************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MeTabContent = void 0;
/**
 * 该页面为测试页面，没有任何作用
 * powered by yiyefangzhou24
 * 2022/5/16
 */
//@Entry
//@Component
//export class  MeTabContent { constructor(compilerAssignedUniqueChildId?, parent?, params?) {}
//
//  private id: string = '10000'
//
//  @State bgColor: Resource = $r('app.color.green0')
//
//  @Builder optionItem(){
//    Row(){
//    }.width('100%').height(60)
//    .backgroundColor(this.bgColor)
//  }
//
//  build() {
//    Column(){
//        this.optionItem()
//    }
//    .width('100%').height('100%')
//  }
//}
class MeTabContent extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/test.ets(35:5)");
        Flex.width('100%');
        Flex.height(60);
        Flex.backgroundColor(Color.Black);
        Image.create({ "id": 16777297, "type": 20000, params: [] });
        Image.debugLine("pages/test.ets(36:7)");
        Image.height(30);
        Image.width(30);
        Image.margin({ left: 15 });
        Image.onClick(() => {
            router.back();
        });
        Image.create({ "id": 16777298, "type": 20000, params: [] });
        Image.debugLine("pages/test.ets(43:7)");
        Image.height(30);
        Image.width(30);
        Image.margin({ right: 15 });
        Flex.pop();
    }
}
exports.MeTabContent = MeTabContent;
loadDocument(new MeTabContent("1", undefined, {}));

})();

/******/ })()
;