/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/emojiLayout.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/emojiLayout.ets ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 该组件为聊天页面的表情插件的页面布局。
 * 表情插件固定高度250vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.emojiLayout = void 0;
__webpack_require__(/*! ../../model/data/EmojiData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/EmojiData.ets");
const EmojiModel_ets_1 = __webpack_require__(/*! ../../model/EmojiModel.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/EmojiModel.ets");
/**
 * emoji顶部Tabbar的数据结构
 * 这里是固定的结构不需要和后台Ability交互，且数据不多，所以没有放在model中
 */
class emojiTabData {
    constructor(index, image, tag) {
        this.index = index;
        this.image = image;
        this.tag = tag;
    }
}
class emojiLayout extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.controller = new TabsController();
        this.__emojiTabIndex = new ObservedPropertySimple(1, this, "emojiTabIndex");
        this.emojiItems = EmojiModel_ets_1.getEmojiDatas() //初始化表情数据
        ;
        this.emojiTabBarItems = [
            {
                "index": 0,
                "image": { "id": 16777328, "type": 20000, params: [] },
                "tag": "搜索"
            },
            {
                "index": 1,
                "image": { "id": 16777300, "type": 20000, params: [] },
                "tag": "表情"
            },
            {
                "index": 2,
                "image": { "id": 16777312, "type": 20000, params: [] },
                "tag": "收藏"
            }
        ];
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.controller !== undefined) {
            this.controller = params.controller;
        }
        if (params.emojiTabIndex !== undefined) {
            this.emojiTabIndex = params.emojiTabIndex;
        }
        if (params.emojiItems !== undefined) {
            this.emojiItems = params.emojiItems;
        }
        if (params.emojiTabBarItems !== undefined) {
            this.emojiTabBarItems = params.emojiTabBarItems;
        }
    }
    aboutToBeDeleted() {
        this.__emojiTabIndex.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get emojiTabIndex() {
        return this.__emojiTabIndex.get();
    }
    set emojiTabIndex(newValue) {
        this.__emojiTabIndex.set(newValue);
    }
    /**
     * 顶部导航栏
     * 布局样式
     */
    emojiTabBar() {
        Row.create();
        Row.debugLine("common/components/emojiLayout.ets(54:5)");
        Row.padding({ left: 10, right: 10 });
        Row.height(50);
        Row.width('100%');
        Row.backgroundColor({ "id": 16777289, "type": 10001, params: [] });
        ForEach.create("1", this, ObservedObject.GetRawObject(this.emojiTabBarItems), item => {
            Column.create();
            Column.debugLine("common/components/emojiLayout.ets(56:9)");
            Column.padding(8);
            Column.height(40);
            Column.width(40);
            Column.borderRadius(8);
            Column.backgroundColor((item.index == this.emojiTabIndex) ? { "id": 16777290, "type": 10001, params: [] } : { "id": 16777289, "type": 10001, params: [] });
            Column.onClick(() => {
                this.controller.changeIndex(item.index);
            });
            Image.create(item.image);
            Image.debugLine("common/components/emojiLayout.ets(57:11)");
            Column.pop();
        }, item => item.index.toString());
        ForEach.pop();
        Row.pop();
    }
    /**
     * emoji表情标签
     * Tab_item
     */
    emojiTabContent() {
        Grid.create();
        Grid.debugLine("common/components/emojiLayout.ets(80:5)");
        Grid.columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr');
        Grid.width('100%');
        Grid.height(200);
        Grid.rowsGap(10);
        Grid.onScrollIndex((first) => {
        });
        Grid.padding({ top: 20 });
        Grid.backgroundColor({ "id": 16777285, "type": 10001, params: [] });
        ForEach.create("2", this, ObservedObject.GetRawObject(this.emojiItems), item => {
            GridItem.create();
            GridItem.debugLine("common/components/emojiLayout.ets(82:9)");
            Image.create($rawfile(item.file));
            Image.debugLine("common/components/emojiLayout.ets(83:11)");
            Image.height(30);
            Image.width(30);
            GridItem.pop();
        }, item => item.tag);
        ForEach.pop();
        Grid.pop();
    }
    /**
     * 搜索表情标签
     * Tab_item
     */
    searchTabContent() {
        Column.create();
        Column.debugLine("common/components/emojiLayout.ets(104:5)");
        Column.height(200);
        Column.width('100%');
        Column.padding({ top: 20 });
        Column.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        TextInput.create({ placeholder: { "id": 16777225, "type": 10003, params: [] } });
        TextInput.debugLine("common/components/emojiLayout.ets(105:7)");
        TextInput.width('90%');
        TextInput.height(30);
        TextInput.type(InputType.Normal);
        TextInput.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        TextInput.fontSize(15);
        TextInput.placeholderFont({ size: 15, family: { "id": 117440980, "type": 10002, params: [] } });
        TextInput.placeholderColor({ "id": 16777287, "type": 10001, params: [] });
        TextInput.caretColor({ "id": 16777283, "type": 10001, params: [] });
        TextInput.backgroundColor({ "id": 16777289, "type": 10001, params: [] });
        TextInput.enterKeyType(EnterKeyType.Search);
        Text.create({ "id": 16777226, "type": 10003, params: [] });
        Text.debugLine("common/components/emojiLayout.ets(116:7)");
        Text.fontSize(15);
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.margin({ top: 10 });
        Text.pop();
        Column.pop();
    }
    render() {
        Column.create();
        Column.debugLine("common/components/emojiLayout.ets(128:5)");
        //顶部导航栏
        this.emojiTabBar();
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.create({ barPosition: BarPosition.Start, index: 1, controller: this.controller });
        Tabs.debugLine("common/components/emojiLayout.ets(135:7)");
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.onChange((index) => {
            this.emojiTabIndex = index;
        });
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.vertical(false);
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.barHeight(0);
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.width('100%');
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.height(200);
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.scrollable(true);
        TabContent.create();
        TabContent.debugLine("common/components/emojiLayout.ets(136:9)");
        this.searchTabContent();
        TabContent.pop();
        TabContent.create();
        TabContent.debugLine("common/components/emojiLayout.ets(139:9)");
        this.emojiTabContent();
        TabContent.pop();
        TabContent.create();
        TabContent.debugLine("common/components/emojiLayout.ets(142:9)");
        Text.create('结合后端逻辑设计');
        Text.debugLine("common/components/emojiLayout.ets(143:11)");
        Text.fontSize(15);
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.margin({ top: 10 });
        Text.pop();
        TabContent.pop();
        /**
       * 表情标签页组件
       * 分别为 搜索/系统表情/搜藏等
       */
        Tabs.pop();
        Column.pop();
    }
}
exports.emojiLayout = emojiLayout;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MessageListItem.ets":
/*!***********************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MessageListItem.ets ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MessageListItem = void 0;
/**
 * 该组件为聊天页面的聊天记录List的一个item布局。
 * MessageList => item
 * powered by yiyefangzhou24
 * 2022/5/4
 */
const MessageData_ets_1 = __webpack_require__(/*! ../../../model/data/MessageData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MessageData.ets");
class MessageListItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.msgItem = undefined;
        this.__imgHeight = new ObservedPropertySimple(0 //图片高度（用于刷新显示正确的图片比例）
        , this, "imgHeight");
        this.__imgWidth = new ObservedPropertySimple(0 //图片宽度（用于刷新现实正确的图片比例）
        /**
         * 消息头像布局
         * 发送消息和接收消息公用该头像布局
         */
        , this, "imgWidth");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.msgItem !== undefined) {
            this.msgItem = params.msgItem;
        }
        if (params.imgHeight !== undefined) {
            this.imgHeight = params.imgHeight;
        }
        if (params.imgWidth !== undefined) {
            this.imgWidth = params.imgWidth;
        }
    }
    aboutToBeDeleted() {
        this.__imgHeight.aboutToBeDeleted();
        this.__imgWidth.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get imgHeight() {
        return this.__imgHeight.get();
    }
    set imgHeight(newValue) {
        this.__imgHeight.set(newValue);
    }
    get imgWidth() {
        return this.__imgWidth.get();
    }
    set imgWidth(newValue) {
        this.__imgWidth.set(newValue);
    }
    /**
     * 消息头像布局
     * 发送消息和接收消息公用该头像布局
     */
    header_layout() {
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/MessageListItem.ets(20:5)");
        Flex.height(60);
        Flex.width(60);
        Image.create({ "id": 0, "type": 30000, params: ['header1.png'] });
        Image.debugLine("common/components/listitem/MessageListItem.ets(22:7)");
        Image.borderRadius(5);
        Image.width(50);
        Image.height(50);
        Flex.pop();
    }
    /**
     * 发送消息布局
     * 该布局包含发送文字、图片、视频、语音
     */
    sendMsgLayout() {
        Flex.create({ direction: FlexDirection.RowReverse, justifyContent: FlexAlign.End });
        Flex.debugLine("common/components/listitem/MessageListItem.ets(33:5)");
        Flex.width('100%');
        Flex.margin({ bottom: 20 });
        //头像
        this.header_layout();
        //消息内容
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.End, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/MessageListItem.ets(37:7)");
        //消息内容
        Flex.layoutWeight(1);
        If.create();
        if (this.msgItem.type == MessageData_ets_1.msgType.Text) { //文本消息
            If.branchId(0);
            Text.create(this.msgItem.context);
            Text.debugLine("common/components/listitem/MessageListItem.ets(39:11)");
            Text.backgroundColor({ "id": 16777284, "type": 10001, params: [] });
            Text.borderRadius(4);
            Text.padding(8);
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(14);
            Text.pop();
            Image.create({ "id": 16777344, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(45:11)");
            Image.height(12);
            Image.width(6);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Pic) { //图片消息
            If.branchId(1);
            //Flex(){
            Image.create($rawfile(this.msgItem.context));
            Image.debugLine("common/components/listitem/MessageListItem.ets(50:13)");
            //Flex(){
            Image.objectFit(ImageFit.Contain);
            //Flex(){
            Image.borderRadius(4);
            //Flex(){
            Image.sourceSize({ width: this.imgWidth, height: this.imgHeight });
            //Flex(){
            Image.onComplete((msg) => {
                this.imgWidth = msg.width;
                this.imgHeight = msg.height;
            });
            //}.padding(6).backgroundColor($r('app.color.green1'))
            Image.create({ "id": 16777344, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(59:11)");
            //}.padding(6).backgroundColor($r('app.color.green1'))
            Image.height(12);
            //}.padding(6).backgroundColor($r('app.color.green1'))
            Image.width(6);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Video) { //视频消息
            If.branchId(2);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Sound) { //语音消息
            If.branchId(3);
            Flex.create({ direction: FlexDirection.RowReverse, alignItems: ItemAlign.Center });
            Flex.debugLine("common/components/listitem/MessageListItem.ets(65:11)");
            Flex.height(46);
            Flex.width(80);
            Flex.padding(6);
            Flex.backgroundColor({ "id": 16777284, "type": 10001, params: [] });
            Image.create({ "id": 16777330, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(66:13)");
            Image.height(20);
            Image.width(20);
            Image.margin({ right: 5 });
            Text.create('4\'\'');
            Text.debugLine("common/components/listitem/MessageListItem.ets(70:13)");
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(16);
            Text.margin({ right: 5 });
            Text.pop();
            Flex.pop();
            Image.create({ "id": 16777344, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(75:11)");
            Image.height(12);
            Image.width(6);
        }
        If.pop();
        //消息内容
        Flex.pop();
        //空白
        Blank.create();
        Blank.debugLine("common/components/listitem/MessageListItem.ets(81:7)");
        //空白
        Blank.width(78);
        //空白
        Blank.pop();
        Flex.pop();
    }
    /**
     * 接收消息布局
     * 该布局包含接收文字、图片、视频、语音
     */
    recvMsgLayout() {
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Start });
        Flex.debugLine("common/components/listitem/MessageListItem.ets(90:5)");
        Flex.width('100%');
        Flex.margin({ bottom: 20 });
        //头像
        this.header_layout();
        //消息内容
        Flex.create({ direction: FlexDirection.RowReverse, justifyContent: FlexAlign.End, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/MessageListItem.ets(94:7)");
        //消息内容
        Flex.layoutWeight(1);
        If.create();
        if (this.msgItem.type == MessageData_ets_1.msgType.Text) { //文本消息
            If.branchId(0);
            Text.create(this.msgItem.context);
            Text.debugLine("common/components/listitem/MessageListItem.ets(96:11)");
            Text.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
            Text.borderRadius(4);
            Text.padding(8);
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(14);
            Text.pop();
            Image.create({ "id": 16777337, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(102:11)");
            Image.height(12);
            Image.width(6);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Pic) { //图片消息
            If.branchId(1);
            //Flex(){
            Image.create($rawfile(this.msgItem.context));
            Image.debugLine("common/components/listitem/MessageListItem.ets(107:13)");
            //Flex(){
            Image.objectFit(ImageFit.Contain);
            //Flex(){
            Image.borderRadius(4);
            //Flex(){
            Image.sourceSize({ width: this.imgWidth, height: this.imgHeight });
            //Flex(){
            Image.onComplete((msg) => {
                this.imgWidth = msg.width;
                this.imgHeight = msg.height;
            });
            //}.padding(6).backgroundColor($r('app.color.white'))
            Image.create({ "id": 16777337, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(116:11)");
            //}.padding(6).backgroundColor($r('app.color.white'))
            Image.height(12);
            //}.padding(6).backgroundColor($r('app.color.white'))
            Image.width(6);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Video) { //视频消息
            If.branchId(2);
        }
        else if (this.msgItem.type == MessageData_ets_1.msgType.Sound) { //语音消息
            If.branchId(3);
            Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center });
            Flex.debugLine("common/components/listitem/MessageListItem.ets(122:11)");
            Flex.height(46);
            Flex.width(80);
            Flex.padding(6);
            Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
            Image.create({ "id": 16777331, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(123:13)");
            Image.height(20);
            Image.width(20);
            Image.margin({ left: 5 });
            Text.create('4\'\'');
            Text.debugLine("common/components/listitem/MessageListItem.ets(127:13)");
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(16);
            Text.margin({ left: 5 });
            Text.pop();
            Flex.pop();
            Image.create({ "id": 16777337, "type": 20000, params: [] });
            Image.debugLine("common/components/listitem/MessageListItem.ets(132:11)");
            Image.height(12);
            Image.width(6);
        }
        If.pop();
        //消息内容
        Flex.pop();
        //空白
        Blank.create();
        Blank.debugLine("common/components/listitem/MessageListItem.ets(138:7)");
        //空白
        Blank.width(78);
        //空白
        Blank.pop();
        Flex.pop();
    }
    render() {
        If.create();
        if (this.msgItem.io == MessageData_ets_1.msgIO.Send) {
            If.branchId(0);
            this.sendMsgLayout();
        }
        else if (this.msgItem.io == MessageData_ets_1.msgIO.Recv) {
            If.branchId(1);
            this.recvMsgLayout();
        }
        If.pop();
    }
}
exports.MessageListItem = MessageListItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/messageBottom.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/messageBottom.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 该组件为聊天页面的底部输入栏布局（包括文字输入、语音输入、表情、更多功能）。
 * 该插件宽度为动态，默认高度为60，打开表情和更多功能选项后高度为310
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.messageBottom = void 0;
const emojiLayout_ets_1 = __webpack_require__(/*! ./emojiLayout.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/emojiLayout.ets");
const moreLayout_ets_1 = __webpack_require__(/*! ./moreLayout.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/moreLayout.ets");
class messageBottom extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__isText = new ObservedPropertySimple(true //输入框布局显隐
        , this, "isText");
        this.__isSound = new ObservedPropertySimple(false //表情布局显隐
        , this, "isSound");
        this.__isEmoji = new ObservedPropertySimple(false //表情布局显隐
        , this, "isEmoji");
        this.__isMore = new ObservedPropertySimple(false //语音输入布局显隐
        , this, "isMore");
        this.__isEmojiSelected = new ObservedPropertySimple(false //切换表情按钮的图标
        , this, "isEmojiSelected");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.isText !== undefined) {
            this.isText = params.isText;
        }
        if (params.isSound !== undefined) {
            this.isSound = params.isSound;
        }
        if (params.isEmoji !== undefined) {
            this.isEmoji = params.isEmoji;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isEmojiSelected !== undefined) {
            this.isEmojiSelected = params.isEmojiSelected;
        }
    }
    aboutToBeDeleted() {
        this.__isText.aboutToBeDeleted();
        this.__isSound.aboutToBeDeleted();
        this.__isEmoji.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isEmojiSelected.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get isText() {
        return this.__isText.get();
    }
    set isText(newValue) {
        this.__isText.set(newValue);
    }
    get isSound() {
        return this.__isSound.get();
    }
    set isSound(newValue) {
        this.__isSound.set(newValue);
    }
    get isEmoji() {
        return this.__isEmoji.get();
    }
    set isEmoji(newValue) {
        this.__isEmoji.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isEmojiSelected() {
        return this.__isEmojiSelected.get();
    }
    set isEmojiSelected(newValue) {
        this.__isEmojiSelected.set(newValue);
    }
    render() {
        Column.create();
        Column.debugLine("common/components/messageBottom.ets(20:5)");
        Column.width('100%');
        /**
         * 输入框布局
         * 包括文字输入和语音输入
         */
        Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/messageBottom.ets(25:7)");
        /**
         * 输入框布局
         * 包括文字输入和语音输入
         */
        Flex.width('100%');
        /**
         * 输入框布局
         * 包括文字输入和语音输入
         */
        Flex.height(60);
        /**
         * 输入框布局
         * 包括文字输入和语音输入
         */
        Flex.backgroundColor({ "id": 16777289, "type": 10001, params: [] });
        //语音按钮
        Image.create({ "id": 16777329, "type": 20000, params: [] });
        Image.debugLine("common/components/messageBottom.ets(27:9)");
        //语音按钮
        Image.height(25);
        //语音按钮
        Image.width(25);
        //语音按钮
        Image.margin({ left: 10 });
        //语音按钮
        Image.onClick(() => {
            this.isSound = !this.isSound;
            this.isText = !this.isText;
            this.isEmoji = false;
            this.isMore = false;
            this.isEmojiSelected = false;
        });
        If.create();
        //输入框（包括文本输入框和语音输入框）
        if (this.isText == true) {
            If.branchId(0);
            TextInput.create();
            TextInput.debugLine("common/components/messageBottom.ets(40:11)");
            TextInput.type(InputType.Normal);
            TextInput.enterKeyType(EnterKeyType.Send);
            TextInput.caretColor({ "id": 16777283, "type": 10001, params: [] });
            TextInput.height(40);
            TextInput.borderRadius('4');
            TextInput.fontSize(16);
            TextInput.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            TextInput.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
            TextInput.margin({ left: 10 });
            TextInput.layoutWeight(1);
        }
        else if (this.isSound == true) {
            If.branchId(1);
            Button.createWithLabel('按住 说话', { type: ButtonType.Normal, stateEffect: true });
            Button.debugLine("common/components/messageBottom.ets(52:11)");
            Button.borderRadius(8);
            Button.fontSize(14);
            Button.fontColor({ "id": 16777281, "type": 10001, params: [] });
            Button.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
            Button.width(100);
            Button.margin({ left: 10, right: 10 });
            Button.layoutWeight(1);
            Button.pop();
        }
        If.pop();
        //emoji按钮
        Image.create((false == this.isEmojiSelected) ? { "id": 16777300, "type": 20000, params: [] } : { "id": 16777311, "type": 20000, params: [] });
        Image.debugLine("common/components/messageBottom.ets(62:9)");
        //emoji按钮
        Image.height(25);
        //emoji按钮
        Image.width(25);
        //emoji按钮
        Image.margin({ left: 10 });
        //emoji按钮
        Image.onClick(() => {
            this.isEmojiSelected = !this.isEmojiSelected;
            this.isSound = false;
            this.isText = true;
            this.isEmoji = !this.isEmoji;
            this.isMore = false;
        });
        //更多功能按钮
        Image.create({ "id": 16777295, "type": 20000, params: [] });
        Image.debugLine("common/components/messageBottom.ets(74:9)");
        //更多功能按钮
        Image.height(25);
        //更多功能按钮
        Image.width(25);
        //更多功能按钮
        Image.margin({ left: 10, right: 20 });
        //更多功能按钮
        Image.onClick(() => {
            this.isSound = false;
            this.isText = true;
            this.isEmoji = false;
            this.isMore = !this.isMore;
            this.isEmojiSelected = false;
        });
        /**
         * 输入框布局
         * 包括文字输入和语音输入
         */
        Flex.pop();
        If.create();
        /**
         * 表情布局
         * 该布局为条件触发，当isEmoji为true显示，否则隐藏
         */
        if (this.isEmoji == true) {
            If.branchId(0);
            Flex.create();
            Flex.debugLine("common/components/messageBottom.ets(95:9)");
            Flex.width('100%');
            Flex.height(250);
            let earlierCreatedChild_1 = this.findChildById("1");
            if (earlierCreatedChild_1 == undefined) {
                View.create(new emojiLayout_ets_1.emojiLayout("1", this, {}));
            }
            else {
                earlierCreatedChild_1.updateWithValueParams({});
                View.create(earlierCreatedChild_1);
            }
            Flex.pop();
        }
        If.pop();
        If.create();
        /**
         * 功能布局
         * 该布局为条件触发，当isMore为true显示，否则隐藏
         */
        if (this.isMore) {
            If.branchId(0);
            Flex.create();
            Flex.debugLine("common/components/messageBottom.ets(103:9)");
            Flex.width('100%');
            Flex.height(250);
            let earlierCreatedChild_2 = this.findChildById("2");
            if (earlierCreatedChild_2 == undefined) {
                View.create(new moreLayout_ets_1.moreLayout("2", this, {}));
            }
            else {
                earlierCreatedChild_2.updateWithValueParams({});
                if (!earlierCreatedChild_2.needsUpdate()) {
                    earlierCreatedChild_2.markStatic();
                }
                View.create(earlierCreatedChild_2);
            }
            Flex.pop();
        }
        If.pop();
        Column.pop();
    }
}
exports.messageBottom = messageBottom;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/moreLayout.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/moreLayout.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为聊天页面的更多功能插件的页面布局。
 * 更多功能插件固定高度250vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.moreLayout = void 0;
/**
 * 功能信息结构体
 * 功能数量较少，且是固定的，故就放在控件中定义
 * @param index
 * @param image
 * @param tag
 */
class funcData {
    constructor(index, image, tag) {
        this.index = index;
        this.image = image;
        this.tag = tag;
    }
}
class moreLayout extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.funcItems = [
            {
                "index": 0,
                "image": { "id": 16777304, "type": 20000, params: [] },
                "tag": "相册"
            },
            {
                "index": 1,
                "image": { "id": 16777306, "type": 20000, params: [] },
                "tag": "拍摄"
            },
            {
                "index": 2,
                "image": { "id": 16777308, "type": 20000, params: [] },
                "tag": "视频通话"
            },
            {
                "index": 3,
                "image": { "id": 16777303, "type": 20000, params: [] },
                "tag": "位置"
            },
            {
                "index": 4,
                "image": { "id": 16777305, "type": 20000, params: [] },
                "tag": "红包"
            },
            {
                "index": 5,
                "image": { "id": 16777307, "type": 20000, params: [] },
                "tag": "转账"
            },
            {
                "index": 6,
                "image": { "id": 16777309, "type": 20000, params: [] },
                "tag": "语音输入"
            },
            {
                "index": 7,
                "image": { "id": 16777302, "type": 20000, params: [] },
                "tag": "我的收藏"
            }
        ];
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.funcItems !== undefined) {
            this.funcItems = params.funcItems;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.create();
        Grid.debugLine("common/components/moreLayout.ets(74:5)");
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.padding(20);
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.width('100%');
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.height(250);
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.columnsTemplate('1fr 1fr 1fr 1fr');
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.rowsTemplate('1fr 1fr');
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.backgroundColor({ "id": 16777289, "type": 10001, params: [] });
        ForEach.create("1", this, ObservedObject.GetRawObject(this.funcItems), item => {
            GridItem.create();
            GridItem.debugLine("common/components/moreLayout.ets(76:9)");
            Column.create();
            Column.debugLine("common/components/moreLayout.ets(77:11)");
            Column.create();
            Column.debugLine("common/components/moreLayout.ets(78:13)");
            Column.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
            Column.borderRadius(10);
            Image.create(item.image);
            Image.debugLine("common/components/moreLayout.ets(79:15)");
            Image.width(60);
            Image.height(60);
            Column.pop();
            Text.create(item.tag);
            Text.debugLine("common/components/moreLayout.ets(85:13)");
            Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
            Text.fontSize(12);
            Text.maxLines(1);
            Text.margin({ top: 5 });
            Text.pop();
            Column.pop();
            GridItem.pop();
        }, item => item.index.toString());
        ForEach.pop();
        /**
         * 功能列表
         * 网格控件还不支持横向滚动 ,坐等更新 ~0.0~
         */
        Grid.pop();
    }
}
exports.moreLayout = moreLayout;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为导航栏，除了特定页面如闪屏页面，都应该有导航栏。
 * 导航栏固定高度60vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.navigationBar = void 0;
var router = globalThis.requireNativeModule('system.router');
class navigationBar extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__title = new ObservedPropertySimple('', this, "title");
        this.__isBack = new ObservedPropertySimple(true, this, "isBack");
        this.__isMenu = new ObservedPropertySimple(true, this, "isMenu");
        this.__isSearch = new ObservedPropertySimple(true, this, "isSearch");
        this.__isMore = new ObservedPropertySimple(false, this, "isMore");
        this.__isClose = new ObservedPropertySimple(false, this, "isClose");
        this.__bgColor = new ObservedPropertyObject({ "id": 16777285, "type": 10001, params: [] }, this, "bgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.title !== undefined) {
            this.title = params.title;
        }
        if (params.isBack !== undefined) {
            this.isBack = params.isBack;
        }
        if (params.isMenu !== undefined) {
            this.isMenu = params.isMenu;
        }
        if (params.isSearch !== undefined) {
            this.isSearch = params.isSearch;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isClose !== undefined) {
            this.isClose = params.isClose;
        }
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
    }
    aboutToBeDeleted() {
        this.__title.aboutToBeDeleted();
        this.__isBack.aboutToBeDeleted();
        this.__isMenu.aboutToBeDeleted();
        this.__isSearch.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isClose.aboutToBeDeleted();
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    get isBack() {
        return this.__isBack.get();
    }
    set isBack(newValue) {
        this.__isBack.set(newValue);
    }
    get isMenu() {
        return this.__isMenu.get();
    }
    set isMenu(newValue) {
        this.__isMenu.set(newValue);
    }
    get isSearch() {
        return this.__isSearch.get();
    }
    set isSearch(newValue) {
        this.__isSearch.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isClose() {
        return this.__isClose.get();
    }
    set isClose(newValue) {
        this.__isClose.set(newValue);
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/navigationBar.ets(21:4)");
        Row.height(60);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(26:7)");
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isBack == true) {
            If.branchId(0);
            Image.create({ "id": 16777296, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(28:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        If.create();
        if (this.isClose == true) {
            If.branchId(0);
            Image.create({ "id": 16777299, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(36:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.pop();
        /**
         * 中间标题栏
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(47:7)");
        /**
         * 中间标题栏
         */
        Column.layoutWeight(1);
        /**
         * 中间标题栏
         */
        Column.padding({ left: 20 });
        Text.create(this.title);
        Text.debugLine("common/components/navigationBar.ets(48:9)");
        Text.fontSize(18);
        Text.fontFamily({ "id": 117440975, "type": 10002, params: [] });
        Text.maxLines(1);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.pop();
        /**
         * 中间标题栏
         */
        Column.pop();
        /**
         * 右侧搜索按钮
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(57:7)");
        /**
         * 右侧搜索按钮
         */
        Column.width(30);
        If.create();
        if (this.isSearch == true) {
            If.branchId(0);
            Image.create({ "id": 16777328, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(59:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧搜索按钮
         */
        Column.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(68:7)");
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isMenu == true) {
            If.branchId(0);
            Image.create({ "id": 16777295, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(70:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        If.create();
        if (this.isMore == true) {
            If.branchId(0);
            Image.create({ "id": 16777322, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(75:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.pop();
        Row.pop();
    }
}
exports.navigationBar = navigationBar;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/EmojiModel.ets":
/*!*********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/EmojiModel.ets ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getEmojiDatas = void 0;
const EmojiData_ets_1 = __webpack_require__(/*! ./data/EmojiData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/EmojiData.ets");
/**
 * 用于初始化EmojiData的数组
 */
function getEmojiDatas() {
    let emojisDataArray = [];
    EmojisComposition.forEach(item => {
        emojisDataArray.push(new EmojiData_ets_1.EmojiData(item.file, item.tag));
    });
    return emojisDataArray;
}
exports.getEmojiDatas = getEmojiDatas;
/**
 * 定义emoji数据测试样例
 */
const EmojisComposition = [
    {
        "file": 'emoji/emoji_01.png',
        "tag": "[微笑]"
    },
    {
        "file": 'emoji/emoji_00.png',
        "tag": "[撇嘴]"
    },
    {
        "file": 'emoji/emoji_02.png',
        "tag": "[色]"
    },
    {
        "file": 'emoji/emoji_03.png',
        "tag": "[发呆]"
    },
    {
        "file": 'emoji/emoji_04.png',
        "tag": "[得意]"
    },
    {
        "file": 'emoji/emoji_05.png',
        "tag": "[流泪]"
    },
    {
        "file": 'emoji/emoji_06.png',
        "tag": "[害羞]"
    },
    {
        "file": 'emoji/emoji_07.png',
        "tag": "[闭嘴]"
    },
    {
        "file": 'emoji/emoji_08.png',
        "tag": "[睡]"
    },
    {
        "file": 'emoji/emoji_09.png',
        "tag": "[大哭]"
    },
    {
        "file": 'emoji/emoji_10.png',
        "tag": "[尴尬]"
    },
    {
        "file": 'emoji/emoji_11.png',
        "tag": "[发怒]"
    },
    {
        "file": 'emoji/emoji_12.png',
        "tag": "[调皮]"
    },
    {
        "file": 'emoji/emoji_13.png',
        "tag": "[龇牙]"
    },
    {
        "file": 'emoji/emoji_14.png',
        "tag": "[惊讶]"
    },
    {
        "file": 'emoji/emoji_15.png',
        "tag": "[难过]"
    },
    {
        "file": 'emoji/emoji_16.png',
        "tag": "[库]"
    },
    {
        "file": 'emoji/emoji_17.png',
        "tag": "[冷汗]"
    },
    {
        "file": 'emoji/emoji_18.png',
        "tag": "[抓狂]"
    },
    {
        "file": 'emoji/emoji_19.png',
        "tag": "[吐]"
    },
    {
        "file": 'emoji/emoji_20.png',
        "tag": "[偷笑]"
    },
    {
        "file": 'emoji/emoji_21.png',
        "tag": "[愉快]"
    },
    {
        "file": 'emoji/emoji_22.png',
        "tag": "[白眼]"
    },
    {
        "file": 'emoji/emoji_23.png',
        "tag": "[傲慢]"
    },
    {
        "file": 'emoji/emoji_24.png',
        "tag": "[饥饿]"
    },
    {
        "file": 'emoji/emoji_25.png',
        "tag": "[困]"
    },
    {
        "file": 'emoji/emoji_26.png',
        "tag": "[惊恐]"
    },
    {
        "file": 'emoji/emoji_27.png',
        "tag": "[流汗]"
    },
    {
        "file": 'emoji/emoji_28.png',
        "tag": "[憨笑]"
    },
    {
        "file": 'emoji/emoji_29.png',
        "tag": "[休闲]"
    },
    {
        "file": 'emoji/emoji_30.png',
        "tag": "[奋斗]"
    },
    {
        "file": 'emoji/emoji_31.png',
        "tag": "[咒骂]"
    },
    {
        "file": 'emoji/emoji_32.png',
        "tag": "[疑问]"
    },
    {
        "file": 'emoji/emoji_33.png',
        "tag": "[嘘]"
    },
    {
        "file": 'emoji/emoji_34.png',
        "tag": "[晕]"
    },
    {
        "file": 'emoji/emoji_35.png',
        "tag": "[疯了]"
    },
    {
        "file": 'emoji/emoji_36.png',
        "tag": "[衰]"
    },
    {
        "file": 'emoji/emoji_37.png',
        "tag": "[骷髅]"
    },
    {
        "file": 'emoji/emoji_38.png',
        "tag": "[敲打]"
    },
    {
        "file": 'emoji/emoji_39.png',
        "tag": "[再见]"
    },
    {
        "file": 'emoji/emoji_40.png',
        "tag": "[擦汗]"
    },
    {
        "file": 'emoji/emoji_41.png',
        "tag": "[抠鼻]"
    },
    {
        "file": 'emoji/emoji_42.png',
        "tag": "[鼓掌]"
    },
    {
        "file": 'emoji/emoji_43.png',
        "tag": "[糗大了]"
    },
    {
        "file": 'emoji/emoji_44.png',
        "tag": "[坏笑]"
    },
    {
        "file": 'emoji/emoji_45.png',
        "tag": "[左哼哼]"
    },
    {
        "file": 'emoji/emoji_46.png',
        "tag": "[右哼哼]"
    },
    {
        "file": 'emoji/emoji_47.png',
        "tag": "[哈欠]"
    },
    {
        "file": 'emoji/emoji_48.png',
        "tag": "[鄙视]"
    },
    {
        "file": 'emoji/emoji_49.png',
        "tag": "[委屈]"
    },
    {
        "file": 'emoji/emoji_50.png',
        "tag": "[快哭]"
    },
    {
        "file": 'emoji/emoji_51.png',
        "tag": "[阴险]"
    },
    {
        "file": 'emoji/emoji_52.png',
        "tag": "[亲亲]"
    },
    {
        "file": 'emoji/emoji_53.png',
        "tag": "[吓]"
    },
    {
        "file": 'emoji/emoji_54.png',
        "tag": "[可怜]"
    },
    {
        "file": 'emoji/emoji_55.png',
        "tag": "[菜刀]"
    },
    {
        "file": 'emoji/emoji_56.png',
        "tag": "[西瓜]"
    },
    {
        "file": 'emoji/emoji_57.png',
        "tag": "[啤酒]"
    },
    {
        "file": 'emoji/emoji_58.png',
        "tag": "[篮球]"
    },
    {
        "file": 'emoji/emoji_59.png',
        "tag": "[乒乓]"
    },
    {
        "file": 'emoji/emoji_60.png',
        "tag": "[咖啡]"
    },
    {
        "file": 'emoji/emoji_61.png',
        "tag": "[饭]"
    },
    {
        "file": 'emoji/emoji_62.png',
        "tag": "[猪头]"
    },
    {
        "file": 'emoji/emoji_63.png',
        "tag": "[玫瑰]"
    },
    {
        "file": 'emoji/emoji_64.png',
        "tag": "[凋谢]"
    },
    {
        "file": 'emoji/emoji_65.png',
        "tag": "[嘴唇]"
    },
    {
        "file": 'emoji/emoji_66.png',
        "tag": "[爱心]"
    },
    {
        "file": 'emoji/emoji_67.png',
        "tag": "[心碎]"
    },
    {
        "file": 'emoji/emoji_68.png',
        "tag": "[蛋糕]"
    },
    {
        "file": 'emoji/emoji_69.png',
        "tag": "[闪电]"
    },
    {
        "file": 'emoji/emoji_70.png',
        "tag": "[炸弹]"
    },
    {
        "file": 'emoji/emoji_71.png',
        "tag": "[刀]"
    },
    {
        "file": 'emoji/emoji_72.png',
        "tag": "[足球]"
    },
    {
        "file": 'emoji/emoji_73.png',
        "tag": "[瓢虫]"
    },
    {
        "file": 'emoji/emoji_74.png',
        "tag": "[便便]"
    },
    {
        "file": 'emoji/emoji_75.png',
        "tag": "[月亮]"
    },
    {
        "file": 'emoji/emoji_76.png',
        "tag": "[太阳]"
    },
    {
        "file": 'emoji/emoji_77.png',
        "tag": "[礼物]"
    },
    {
        "file": 'emoji/emoji_78.png',
        "tag": "[拥抱]"
    },
    {
        "file": 'emoji/emoji_79.png',
        "tag": "[强]"
    },
    {
        "file": 'emoji/emoji_80.png',
        "tag": "[弱]"
    },
    {
        "file": 'emoji/emoji_81.png',
        "tag": "[握手]"
    },
    {
        "file": 'emoji/emoji_82.png',
        "tag": "[胜利]"
    },
    {
        "file": 'emoji/emoji_83.png',
        "tag": "[抱拳]"
    },
    {
        "file": 'emoji/emoji_84.png',
        "tag": "[勾引]"
    },
    {
        "file": 'emoji/emoji_85.png',
        "tag": "[拳头]"
    },
    {
        "file": 'emoji/emoji_86.png',
        "tag": "[差劲]"
    },
    {
        "file": 'emoji/emoji_87.png',
        "tag": "[爱你]"
    },
    {
        "file": 'emoji/emoji_88.png',
        "tag": "[NO]"
    },
    {
        "file": 'emoji/emoji_89.png',
        "tag": "[OK]"
    },
    {
        "file": 'emoji/emoji_90.png',
        "tag": "[爱情]"
    },
    {
        "file": 'emoji/emoji_91.png',
        "tag": "[飞吻]"
    },
    {
        "file": 'emoji/emoji_92.png',
        "tag": "[跳跳]"
    },
    {
        "file": 'emoji/emoji_93.png',
        "tag": "[发抖]"
    },
    {
        "file": 'emoji/emoji_94.png',
        "tag": "[怄火]"
    },
    {
        "file": 'emoji/emoji_95.png',
        "tag": "[转圈]"
    },
    {
        "file": 'emoji/emoji_96.png',
        "tag": "[磕头]"
    },
    {
        "file": 'emoji/emoji_97.png',
        "tag": "[回头]"
    },
    {
        "file": 'emoji/emoji_98.png',
        "tag": "[跳绳]"
    },
    {
        "file": 'emoji/emoji_99.png',
        "tag": "[投降]"
    },
    {
        "file": 'emoji/emoji_100.png',
        "tag": "[激动]"
    },
    {
        "file": 'emoji/emoji_101.png',
        "tag": "[乱舞]"
    },
    {
        "file": 'emoji/emoji_102.png',
        "tag": "[献吻]"
    },
    {
        "file": 'emoji/emoji_103.png',
        "tag": "[左太极]"
    },
    {
        "file": 'emoji/emoji_104.png',
        "tag": "[右太极]"
    }
];


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MessageModel.ets":
/*!***********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MessageModel.ets ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 定义聊天消息界面的数据样例
 * 该模型与ability对接
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getMessagesDatas = void 0;
const MessageData_ets_1 = __webpack_require__(/*! ./data/MessageData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MessageData.ets");
/**
 * 用于初始化MessageData的数组
 */
function getMessagesDatas() {
    let msgsDataArray = [];
    MessagesComposition.forEach(item => {
        msgsDataArray.push(new MessageData_ets_1.MessageData(item.mid, item.wid, item.io, item.context, item.type, item.time));
    });
    msgsDataArray.forEach(item => {
        console.log('mid:' + item.mid + '|wid:' + item.wid + '|context:' + item.context);
        if (item.io == MessageData_ets_1.msgIO.Send) {
            console.log('send');
        }
        else {
            console.log('recv');
        }
    });
    return msgsDataArray;
}
exports.getMessagesDatas = getMessagesDatas;
/**
 * 定义聊天列表数据测试样例
 */
const MessagesComposition = [
    {
        "mid": "084659852ba2049c4acf02cb39c4aad6",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Send,
        "context": "念刘备、关羽、张飞，虽然异姓，既结为兄弟，则同心协力，救困扶危；上报国家，下安黎庶。",
        "type": MessageData_ets_1.msgType.Text,
        "time": 1652056345
    },
    {
        "mid": "0baf6a65242f9276ebb0d44d0a0dda3b",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Recv,
        "context": "不求同年同月同日生，只愿同年同月同日死。",
        "type": MessageData_ets_1.msgType.Text,
        "time": 1652056355
    },
    {
        "mid": "40ebf2bf2089efade574fb3534e224cb",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Send,
        "context": "皇天后土，实鉴此心，背义忘恩，天人共戮！",
        "type": MessageData_ets_1.msgType.Text,
        "time": 1652056365
    },
    {
        "mid": "03603ea3f57acce157caf7f31af01724",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Send,
        "context": "sample_picture.jpeg",
        "type": MessageData_ets_1.msgType.Pic,
        "time": 1652056375
    },
    {
        "mid": "03603ea3f57acce157caf7f31af01725",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Recv,
        "context": "sample_picture.jpeg",
        "type": MessageData_ets_1.msgType.Pic,
        "time": 1652056376
    },
    {
        "mid": "8ca1e2e5e711fad8ea6a1250457629a4",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Recv,
        "context": "sample_video.mp4",
        "type": MessageData_ets_1.msgType.Video,
        "time": 1652056385
    },
    {
        "mid": "72edec50c82cf82d2d4d4ed31435d3f8",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Send,
        "context": "sample_sound.mp3",
        "type": MessageData_ets_1.msgType.Sound,
        "time": 1652056395
    },
    {
        "mid": "72edec50c82cf82d2d4d4ed31435d3f9",
        "wid": "10001",
        "io": MessageData_ets_1.msgIO.Recv,
        "context": "sample_sound.mp3",
        "type": MessageData_ets_1.msgType.Sound,
        "time": 1652056396
    }
];


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/EmojiData.ets":
/*!*************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/EmojiData.ets ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * emoji表情数据结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.EmojiData = void 0;
class EmojiData {
    constructor(file, tag) {
        this.file = file;
        this.tag = tag;
    }
}
exports.EmojiData = EmojiData;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MessageData.ets":
/*!***************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MessageData.ets ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 聊天页面聊天记录结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MessageData = exports.msgType = exports.msgIO = void 0;
/**
 * 消息流向
 * 主要有：发送/接收
 */
var msgIO;
(function (msgIO) {
    //发送
    msgIO[msgIO["Send"] = 0] = "Send";
    //接收
    msgIO[msgIO["Recv"] = 1] = "Recv";
})(msgIO = exports.msgIO || (exports.msgIO = {}));
/**
 * 消息类型
 * 主要有：文本消息/图片消息/视频/语音
 */
var msgType;
(function (msgType) {
    //文本消息
    msgType[msgType["Text"] = 0] = "Text";
    //图片消息
    msgType[msgType["Pic"] = 1] = "Pic";
    //语音消息
    msgType[msgType["Sound"] = 2] = "Sound";
    //视频消息
    msgType[msgType["Video"] = 3] = "Video";
})(msgType = exports.msgType || (exports.msgType = {}));
class MessageData {
    constructor(mid, wid, io, context, type, time) {
        this.mid = mid;
        this.wid = wid;
        this.io = io;
        this.context = context;
        this.type = type;
        this.time = time;
    }
}
exports.MessageData = MessageData;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets":
/*!***************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 主界面会话列表的数据结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SessionData = void 0;
class SessionData {
    constructor(wid, headImg, name, lastMsg, unReadMsg) {
        this.wid = wid;
        this.headImg = headImg;
        this.name = name;
        this.lastMsg = lastMsg;
        this.unReadMsg = unReadMsg;
    }
}
exports.SessionData = SessionData;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Message.ets?entry ***!
  \************************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为聊天页面。
 * powered by yiyefangzhou24
 * 2022/5/14
 */
const navigationBar_ets_1 = __webpack_require__(/*! ../common/components/navigationBar.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets");
__webpack_require__(/*! ../model/data/MessageData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/MessageData.ets");
__webpack_require__(/*! ../model/data/SessionData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets");
const MessageModel_ets_1 = __webpack_require__(/*! ../model/MessageModel.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/MessageModel.ets");
const MessageListItem_ets_1 = __webpack_require__(/*! ../common/components/listitem/MessageListItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/MessageListItem.ets");
const messageBottom_ets_1 = __webpack_require__(/*! ../common/components/messageBottom.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/messageBottom.ets");
var router = globalThis.requireNativeModule('system.router');
class Message extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.msgsItems = MessageModel_ets_1.getMessagesDatas() //初始化聊天记录
        ;
        this.sessionData = router.getParams().sessionData;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.msgsItems !== undefined) {
            this.msgsItems = params.msgsItems;
        }
        if (params.sessionData !== undefined) {
            this.sessionData = params.sessionData;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.Start, justifyContent: FlexAlign.SpaceBetween });
        Flex.debugLine("pages/Message.ets(23:5)");
        Flex.width('100%');
        Flex.height('100%');
        Flex.backgroundColor({ "id": 16777285, "type": 10001, params: [] });
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            /**
             * 导航栏
             * 详见navigationBar.ets
             */
            View.create(new navigationBar_ets_1.navigationBar("2", this, { title: this.sessionData.name, isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: true }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                title: this.sessionData.name, isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: true
            });
            View.create(earlierCreatedChild_2);
        }
        /**
          * 聊天记录列表
          */
        List.create();
        List.debugLine("pages/Message.ets(33:7)");
        /**
          * 聊天记录列表
          */
        List.padding({ top: 10, bottom: 10 });
        /**
          * 聊天记录列表
          */
        List.width('100%');
        ForEach.create("4", this, ObservedObject.GetRawObject(this.msgsItems), item => {
            ListItem.create();
            ListItem.debugLine("pages/Message.ets(35:11)");
            let earlierCreatedChild_3 = this.findChildById("3");
            if (earlierCreatedChild_3 == undefined) {
                View.create(new MessageListItem_ets_1.MessageListItem("3", this, { msgItem: item }));
            }
            else {
                earlierCreatedChild_3.updateWithValueParams({
                    msgItem: item
                });
                View.create(earlierCreatedChild_3);
            }
            ListItem.pop();
        }, item => item.mid);
        ForEach.pop();
        /**
          * 聊天记录列表
          */
        List.pop();
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            /**
             * 底部输入框
             */
            View.create(new messageBottom_ets_1.messageBottom("5", this, {}));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({});
            View.create(earlierCreatedChild_5);
        }
        Flex.pop();
    }
}
loadDocument(new Message("1", undefined, {}));

})();

/******/ })()
;