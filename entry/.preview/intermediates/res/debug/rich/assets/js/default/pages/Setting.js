/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为导航栏，除了特定页面如闪屏页面，都应该有导航栏。
 * 导航栏固定高度60vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.navigationBar = void 0;
var router = globalThis.requireNativeModule('system.router');
class navigationBar extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__title = new ObservedPropertySimple('', this, "title");
        this.__isBack = new ObservedPropertySimple(true, this, "isBack");
        this.__isMenu = new ObservedPropertySimple(true, this, "isMenu");
        this.__isSearch = new ObservedPropertySimple(true, this, "isSearch");
        this.__isMore = new ObservedPropertySimple(false, this, "isMore");
        this.__isClose = new ObservedPropertySimple(false, this, "isClose");
        this.__bgColor = new ObservedPropertyObject({ "id": 16777285, "type": 10001, params: [] }, this, "bgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.title !== undefined) {
            this.title = params.title;
        }
        if (params.isBack !== undefined) {
            this.isBack = params.isBack;
        }
        if (params.isMenu !== undefined) {
            this.isMenu = params.isMenu;
        }
        if (params.isSearch !== undefined) {
            this.isSearch = params.isSearch;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isClose !== undefined) {
            this.isClose = params.isClose;
        }
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
    }
    aboutToBeDeleted() {
        this.__title.aboutToBeDeleted();
        this.__isBack.aboutToBeDeleted();
        this.__isMenu.aboutToBeDeleted();
        this.__isSearch.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isClose.aboutToBeDeleted();
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    get isBack() {
        return this.__isBack.get();
    }
    set isBack(newValue) {
        this.__isBack.set(newValue);
    }
    get isMenu() {
        return this.__isMenu.get();
    }
    set isMenu(newValue) {
        this.__isMenu.set(newValue);
    }
    get isSearch() {
        return this.__isSearch.get();
    }
    set isSearch(newValue) {
        this.__isSearch.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isClose() {
        return this.__isClose.get();
    }
    set isClose(newValue) {
        this.__isClose.set(newValue);
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/navigationBar.ets(21:4)");
        Row.height(60);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(26:7)");
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isBack == true) {
            If.branchId(0);
            Image.create({ "id": 16777296, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(28:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        If.create();
        if (this.isClose == true) {
            If.branchId(0);
            Image.create({ "id": 16777299, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(36:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.pop();
        /**
         * 中间标题栏
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(47:7)");
        /**
         * 中间标题栏
         */
        Column.layoutWeight(1);
        /**
         * 中间标题栏
         */
        Column.padding({ left: 20 });
        Text.create(this.title);
        Text.debugLine("common/components/navigationBar.ets(48:9)");
        Text.fontSize(18);
        Text.fontFamily({ "id": 117440975, "type": 10002, params: [] });
        Text.maxLines(1);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.pop();
        /**
         * 中间标题栏
         */
        Column.pop();
        /**
         * 右侧搜索按钮
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(57:7)");
        /**
         * 右侧搜索按钮
         */
        Column.width(30);
        If.create();
        if (this.isSearch == true) {
            If.branchId(0);
            Image.create({ "id": 16777328, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(59:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧搜索按钮
         */
        Column.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(68:7)");
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isMenu == true) {
            If.branchId(0);
            Image.create({ "id": 16777295, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(70:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        If.create();
        if (this.isMore == true) {
            If.branchId(0);
            Image.create({ "id": 16777322, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(75:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.pop();
        Row.pop();
    }
}
exports.navigationBar = navigationBar;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets":
/*!****************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为设置和其他类似UI的选项布局。
 * 主要内容为图片+文字+导航箭头
 * powered by yiyefangzhou24
 * 2022/5/15
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.optionItem = void 0;
class optionItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__bgColor = new ObservedPropertyObject({ "id": 16777290, "type": 10001, params: [] }, this, "bgColor");
        this.img = undefined;
        this.context = undefined;
        this.isNext = undefined;
        this.marginTop = undefined;
        this.itemOnClick = (event) => { } //item的单击事件（可选）
        ;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
        if (params.img !== undefined) {
            this.img = params.img;
        }
        if (params.context !== undefined) {
            this.context = params.context;
        }
        if (params.isNext !== undefined) {
            this.isNext = params.isNext;
        }
        if (params.marginTop !== undefined) {
            this.marginTop = params.marginTop;
        }
        if (params.itemOnClick !== undefined) {
            this.itemOnClick = params.itemOnClick;
        }
    }
    aboutToBeDeleted() {
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(19:5)");
        Row.width('100%');
        Row.height(60);
        Row.margin({ top: this.marginTop });
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        Row.onTouch((event) => {
            if (event.type === TouchType.Down) {
                this.bgColor = { "id": 16777287, "type": 10001, params: [] };
            }
            if (event.type === TouchType.Up) {
                this.bgColor = { "id": 16777290, "type": 10001, params: [] };
            }
        });
        Row.onClick((event) => {
            this.itemOnClick(event);
        });
        If.create();
        if (this.img != null) {
            If.branchId(0);
            Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center });
            Flex.debugLine("common/utils/optionItem.ets(21:9)");
            Flex.width(50);
            Flex.height(60);
            Image.create(this.img);
            Image.debugLine("common/utils/optionItem.ets(22:11)");
            Image.width(30);
            Image.height(30);
            Flex.pop();
        }
        If.pop();
        Column.create();
        Column.debugLine("common/utils/optionItem.ets(26:7)");
        Column.padding({ left: 10, right: 4 });
        Column.height(60);
        Column.layoutWeight(1);
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(27:9)");
        Row.height(59);
        Text.create(this.context);
        Text.debugLine("common/utils/optionItem.ets(28:11)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(19);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.layoutWeight(1);
        Text.pop();
        If.create();
        //.margin({left: 10})
        if (this.isNext == true) {
            If.branchId(0);
            Image.create({ "id": 16777324, "type": 20000, params: [] });
            Image.debugLine("common/utils/optionItem.ets(36:13)");
            Image.width(18);
            Image.height(18);
            Image.margin(10);
        }
        If.pop();
        Row.pop();
        Divider.create();
        Divider.debugLine("common/utils/optionItem.ets(42:9)");
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        Divider.strokeWidth(0.8);
        Column.pop();
        Row.pop();
    }
}
exports.optionItem = optionItem;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Setting.ets?entry ***!
  \************************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为设置页面。
 * powered by yiyefangzhou24
 * 2022/5/16
 */
const navigationBar_ets_1 = __webpack_require__(/*! ../common/components/navigationBar.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets");
const optionItem_ets_1 = __webpack_require__(/*! ../common/utils/optionItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets");
class Setting extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.scroller = new Scroller();
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.scroller !== undefined) {
            this.scroller = params.scroller;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Column.create();
        Column.debugLine("pages/Setting.ets(15:5)");
        Column.width('100%');
        Column.height('100%');
        Column.backgroundColor({ "id": 16777287, "type": 10001, params: [] });
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            View.create(new navigationBar_ets_1.navigationBar("2", this, { title: { "id": 16777254, "type": 10003, params: [] }, isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: false }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                title: { "id": 16777254, "type": 10003, params: [] },
                isBack: true, isClose: false, isMenu: false, isSearch: false, isMore: false
            });
            View.create(earlierCreatedChild_2);
        }
        /**
       * 功能列表
       */
        Scroll.create(this.scroller);
        Scroll.debugLine("pages/Setting.ets(20:7)");
        /**
       * 功能列表
       */
        Scroll.scrollable(ScrollDirection.Vertical);
        /**
       * 功能列表
       */
        Scroll.scrollBar(BarState.Off);
        Column.create();
        Column.debugLine("pages/Setting.ets(21:9)");
        Column.alignItems(HorizontalAlign.Start);
        let earlierCreatedChild_3 = this.findChildById("3");
        if (earlierCreatedChild_3 == undefined) {
            //账号与安全
            View.create(new optionItem_ets_1.optionItem("3", this, { context: { "id": 16777274, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_3.updateWithValueParams({
                context: { "id": 16777274, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_3);
        }
        let earlierCreatedChild_4 = this.findChildById("4");
        if (earlierCreatedChild_4 == undefined) {
            //青少年模式
            View.create(new optionItem_ets_1.optionItem("4", this, { context: { "id": 16777275, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_4.updateWithValueParams({
                context: { "id": 16777275, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_4);
        }
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            //关怀模式
            View.create(new optionItem_ets_1.optionItem("5", this, { context: { "id": 16777264, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({
                context: { "id": 16777264, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_5);
        }
        let earlierCreatedChild_6 = this.findChildById("6");
        if (earlierCreatedChild_6 == undefined) {
            //新消息通知
            View.create(new optionItem_ets_1.optionItem("6", this, { context: { "id": 16777271, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_6.updateWithValueParams({
                context: { "id": 16777271, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_6);
        }
        let earlierCreatedChild_7 = this.findChildById("7");
        if (earlierCreatedChild_7 == undefined) {
            //聊天
            View.create(new optionItem_ets_1.optionItem("7", this, { context: { "id": 16777265, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_7.updateWithValueParams({
                context: { "id": 16777265, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_7);
        }
        let earlierCreatedChild_8 = this.findChildById("8");
        if (earlierCreatedChild_8 == undefined) {
            //通用
            View.create(new optionItem_ets_1.optionItem("8", this, { context: { "id": 16777268, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_8.updateWithValueParams({
                context: { "id": 16777268, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_8);
        }
        Text.create({ "id": 16777273, "type": 10003, params: [] });
        Text.debugLine("pages/Setting.ets(35:11)");
        Text.fontSize(14);
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.margin({ top: 20, left: 10 });
        Text.pop();
        let earlierCreatedChild_9 = this.findChildById("9");
        if (earlierCreatedChild_9 == undefined) {
            //朋友权限
            View.create(new optionItem_ets_1.optionItem("9", this, { context: { "id": 16777267, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_9.updateWithValueParams({
                context: { "id": 16777267, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_9);
        }
        let earlierCreatedChild_10 = this.findChildById("10");
        if (earlierCreatedChild_10 == undefined) {
            //个人隐私政策
            View.create(new optionItem_ets_1.optionItem("10", this, { context: { "id": 16777272, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_10.updateWithValueParams({
                context: { "id": 16777272, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_10);
        }
        let earlierCreatedChild_11 = this.findChildById("11");
        if (earlierCreatedChild_11 == undefined) {
            //关于微信
            View.create(new optionItem_ets_1.optionItem("11", this, { context: { "id": 16777263, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_11.updateWithValueParams({
                context: { "id": 16777263, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_11);
        }
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/Setting.ets(61:11)");
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.width('100%');
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.height(60);
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.margin({ top: 10 });
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Text.create({ "id": 16777270, "type": 10003, params: [] });
        Text.debugLine("pages/Setting.ets(62:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(19);
        Text.maxLines(1);
        Text.pop();
        /**
             * 目前Button控件貌似不支持width设置为100%
             * 该按钮需要顶格，故更换其他组建
             */
        //        //注销
        //        Button($r('app.string.set_logout'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //        //退出
        //        Button($r('app.string.set_exit'), { type: ButtonType.Normal })
        //          .width('100%').height(60)
        //          .backgroundColor($r('app.color.white'))
        //注销按钮
        Flex.pop();
        //退出按钮
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/Setting.ets(70:11)");
        //退出按钮
        Flex.width('100%');
        //退出按钮
        Flex.height(60);
        //退出按钮
        Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        //退出按钮
        Flex.margin({ top: 10, bottom: 60 });
        Text.create({ "id": 16777266, "type": 10003, params: [] });
        Text.debugLine("pages/Setting.ets(71:13)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(19);
        Text.maxLines(1);
        Text.pop();
        //退出按钮
        Flex.pop();
        Column.pop();
        /**
       * 功能列表
       */
        Scroll.pop();
        Column.pop();
    }
}
loadDocument(new Setting("1", undefined, {}));

})();

/******/ })()
;