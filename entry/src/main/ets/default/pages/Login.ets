/**
 * 该组件为登陆页面。
 * powered by yiyefangzhou24
 * 2022/5/14
 */

import { navigationBar } from '../common/components/navigationBar.ets'
import router from '@system.router'

@Extend(Text) function text_style() {
  .width('25%')
  .fontFamily($r('sys.float.id_text_size_body2'))
  .fontSize(15)
  .maxLines(1)
}

@Extend(Text) function link_text_style(fontSize : number) {
  .fontSize(fontSize)
  .fontColor($r('app.color.blue_link'))
  .fontFamily($r('sys.float.id_text_size_body2'))
  .maxLines(1)
}

@Extend(TextInput) function input_style() {
  .width('75%')
  .fontFamily($r('sys.float.id_text_size_body2'))
  .fontSize(15)
  .placeholderFont({size: 15 , family: $r('sys.float.id_text_size_body2')})
  .placeholderColor($r('app.color.grey3'))
  .caretColor($r('app.color.green0'))
  .backgroundColor(Color.White)
}

@Extend(Divider) function divider_style() {
  .width('90%')
  .color($r('app.color.grey2'))
  .strokeWidth(0.8)
}


@Entry
@Component
struct Login {
  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Start , justifyContent: FlexAlign.SpaceBetween}){
      /**
       * 导航栏
       * 详见navigationBar.ets
       */
      navigationBar({isBack: false , isClose: true , isMenu: false , isSearch: false , isMore: false})
      /**
       * 标题栏
       * 固定高度180vp
       */
      Column() {
        Text($r('app.string.login_title'))
          .textAlign(TextAlign.Center)
          .fontSize(22)
          .fontFamily($r('sys.float.id_text_size_headline7'))
      }.width('100%').height(90).padding({top: 55})
      /**
       * 国家+手机号码输入框
       * 固定高度400vp
       */
      Flex({ direction: FlexDirection.Column , alignItems: ItemAlign.Center}) {
        //国家/地区
        Row(){
          Text($r('app.string.login_country'))
            .text_style()
          TextInput({ placeholder:  $r('app.string.login_country_tips')})
            .input_style()
        }.width('90%').height(60).margin({bottom: 4 , top:4})
        Divider().divider_style()
        //手机号
        Row(){
          Text($r('app.string.login_phone'))
            .text_style()
          TextInput({ placeholder:  $r('app.string.login_phone_tips')})
            .input_style()
            .type(InputType.Number)
        }.width('90%').height(60).margin({bottom: 4 })
        Divider().divider_style()
        //使用其他方式登陆提示
        Row(){
          Text($r('app.string.login_switch'))
            .link_text_style(15)
        }.width('90%').height(50).margin({left: 2 , top: 10})
      }.width('100%').height(400).padding({top: 20})
      /**
       * 底部登陆按钮和说明
       * 底部对齐方式
       */
      Flex({ direction: FlexDirection.Column ,justifyContent: FlexAlign.End, alignItems: ItemAlign.Center}) {
        //按钮提示
        Text($r('app.string.login_button_tips'))
          .fontColor($r('app.color.grey3'))
          .maxLines(1)
          .fontSize(15)
        //登陆按钮
        Button($r('app.string.login_button'), { type: ButtonType.Normal })
          .width(170)
          .height(50)
          .backgroundColor($r("app.color.green0"))
          .fontSize(18)
          .fontColor($r("app.color.white"))
          .borderRadius(8)
          .margin({ top: 30 })
          .onClick(() => {
            router.push({ uri: 'pages/Main' })
          })
        //底部说明页脚
        Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.End, justifyContent: FlexAlign.Center }) {
          Text($r('app.string.login_repass'))
            .link_text_style(12)
            .margin({ right: 5 })
          Text('|')
            .link_text_style(12)
            .margin({ right: 5 })
          Text($r('app.string.login_freeze'))
            .link_text_style(12)
            .margin({ right: 5 })
          Text('|')
            .link_text_style(12)
            .margin({ right: 5 })
          Text($r('app.string.login_security'))
            .link_text_style(12)
        }.width('100%').padding({bottom: 30 , top: 60})
      }.width('100%').layoutWeight(1)
    }.width('100%').height('100%').backgroundColor($r('app.color.grey0'))
  }
}