/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Splash.ets?entry ***!
  \***********************************************************************************************/
/**
 * 该组件为闪屏页。
 * powered by yiyefangzhou24
 * 2022/5/14
 */
var router = globalThis.requireNativeModule('system.router');
class Splash extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Stack.create({ alignContent: Alignment.BottomStart });
        Stack.debugLine("pages/Splash.ets(12:5)");
        Stack.width('100%');
        Stack.height('100%');
        /**
         * 背景图片
         * 页面布局采用堆叠布局，背景铺满整个页面
         */
        Image.create({ "id": 16777345, "type": 20000, params: [] });
        Image.debugLine("pages/Splash.ets(17:7)");
        /**
         * 背景图片
         * 页面布局采用堆叠布局，背景铺满整个页面
         */
        Image.width('100%');
        /**
         * 背景图片
         * 页面布局采用堆叠布局，背景铺满整个页面
         */
        Image.width('100%');
        /**
         * 底部登陆和注册按钮
         */
        Flex.create({ justifyContent: FlexAlign.SpaceBetween });
        Flex.debugLine("pages/Splash.ets(23:7)");
        /**
         * 底部登陆和注册按钮
         */
        Flex.width('100%');
        /**
         * 底部登陆和注册按钮
         */
        Flex.height(90);
        /**
         * 底部登陆和注册按钮
         */
        Flex.padding(20);
        Button.createWithLabel({ "id": 16777276, "type": 10003, params: [] }, { type: ButtonType.Normal });
        Button.debugLine("pages/Splash.ets(24:9)");
        Button.width(120);
        Button.height(50);
        Button.backgroundColor({ "id": 16777283, "type": 10001, params: [] });
        Button.fontSize(18);
        Button.fontColor({ "id": 16777290, "type": 10001, params: [] });
        Button.borderRadius(10);
        Button.onClick(() => {
            router.push({ uri: 'pages/Login' });
        });
        Button.pop();
        Button.createWithLabel({ "id": 16777277, "type": 10003, params: [] }, { type: ButtonType.Normal });
        Button.debugLine("pages/Splash.ets(34:9)");
        Button.width(120);
        Button.height(50);
        Button.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Button.fontSize(18);
        Button.fontColor({ "id": 16777283, "type": 10001, params: [] });
        Button.borderRadius(10);
        Button.onClick(() => {
            router.push({ uri: 'pages/Register' });
        });
        Button.pop();
        /**
         * 底部登陆和注册按钮
         */
        Flex.pop();
        Stack.pop();
    }
}
loadDocument(new Splash("1", undefined, {}));

/******/ })()
;