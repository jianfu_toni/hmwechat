/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/bottomTabs.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/bottomTabs.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为底部标签栏，主界面使用。
 * 导航栏固定高度78vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.BottomTabs = void 0;
/**
 * 底部标签页每个项数据
 * 这段数据是固定不变的，无需暴露model接口，就放在该文件中
 * @param imgSrcNomral 按钮图片
 * @param imgSrcPress 按下后图片
 * @param tabText 按钮文字
 */
class TabItem {
    constructor(imgSrcNomral, imgSrcPress, tabText) {
        this.imgSrcNomral = imgSrcNomral;
        this.imgSrcPress = imgSrcPress;
        this.tabText = tabText;
    }
}
/**
 * 用于初始化TabItem的数组
 */
function getTabItems() {
    let itemsArray = [];
    itemsArray.push(new TabItem({ "id": 16777341, "type": 20000, params: [] }, { "id": 16777342, "type": 20000, params: [] }, { "id": 16777219, "type": 10003, params: [] }));
    itemsArray.push(new TabItem({ "id": 16777291, "type": 20000, params: [] }, { "id": 16777292, "type": 20000, params: [] }, { "id": 16777216, "type": 10003, params: [] }));
    itemsArray.push(new TabItem({ "id": 16777293, "type": 20000, params: [] }, { "id": 16777294, "type": 20000, params: [] }, { "id": 16777217, "type": 10003, params: [] }));
    itemsArray.push(new TabItem({ "id": 16777338, "type": 20000, params: [] }, { "id": 16777339, "type": 20000, params: [] }, { "id": 16777218, "type": 10003, params: [] }));
    return itemsArray;
}
class BottomTabs extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.tabSrc = [0, 1, 2, 3];
        this.controller = new TabsController();
        this.__bottomTabIndex = new SynchedPropertySimpleTwoWay(params.bottomTabIndex, this, "bottomTabIndex");
        this.tabItemArray = getTabItems();
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.tabSrc !== undefined) {
            this.tabSrc = params.tabSrc;
        }
        if (params.controller !== undefined) {
            this.controller = params.controller;
        }
        if (params.tabItemArray !== undefined) {
            this.tabItemArray = params.tabItemArray;
        }
    }
    aboutToBeDeleted() {
        this.__bottomTabIndex.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get bottomTabIndex() {
        return this.__bottomTabIndex.get();
    }
    set bottomTabIndex(newValue) {
        this.__bottomTabIndex.set(newValue);
    }
    render() {
        Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceEvenly });
        Flex.debugLine("common/components/bottomTabs.ets(48:5)");
        Flex.width('100%');
        Flex.height(74);
        Flex.backgroundColor({ "id": 16777285, "type": 10001, params: [] });
        ForEach.create("1", this, ObservedObject.GetRawObject(this.tabSrc), item => {
            Column.create();
            Column.debugLine("common/components/bottomTabs.ets(50:9)");
            Column.onClick(() => {
                this.controller.changeIndex(item);
            });
            Image.create((this.bottomTabIndex == item) ? this.tabItemArray[item].imgSrcPress : this.tabItemArray[item].imgSrcNomral);
            Image.debugLine("common/components/bottomTabs.ets(51:11)");
            Image.objectFit(ImageFit.Contain);
            Image.width(30);
            Image.height(30);
            Text.create(this.tabItemArray[item].tabText);
            Text.debugLine("common/components/bottomTabs.ets(54:11)");
            Text.fontSize(14);
            Text.fontFamily({ "id": 117440981, "type": 10002, params: [] });
            Text.margin({ top: 3 });
            Text.pop();
            Column.pop();
        }, item => item.toString());
        ForEach.pop();
        Flex.pop();
    }
}
exports.BottomTabs = BottomTabs;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/ContactListItem.ets":
/*!***********************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/ContactListItem.ets ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 该组件为主界面第二个标签页面的item布局。
 * ContactList => item
 * powered by yiyefangzhou24
 * 2022/5/14
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ContactListItem = void 0;
__webpack_require__(/*! ../../../model/data/ContactData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets");
var router = globalThis.requireNativeModule('system.router');
class ContactListItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.contactItem = undefined;
        this.__itemBgColor = new ObservedPropertyObject({ "id": 16777290, "type": 10001, params: [] }, this, "itemBgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.contactItem !== undefined) {
            this.contactItem = params.contactItem;
        }
        if (params.itemBgColor !== undefined) {
            this.itemBgColor = params.itemBgColor;
        }
    }
    aboutToBeDeleted() {
        this.__itemBgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get itemBgColor() {
        return this.__itemBgColor.get();
    }
    set itemBgColor(newValue) {
        this.__itemBgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/listitem/ContactListItem.ets(17:5)");
        Row.height(70);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.itemBgColor));
        Row.onTouch((event) => {
            if (event.type === TouchType.Down) {
                this.itemBgColor = { "id": 16777287, "type": 10001, params: [] };
            }
            if (event.type === TouchType.Up) {
                this.itemBgColor = { "id": 16777290, "type": 10001, params: [] };
            }
        });
        Row.onClick(() => {
            router.push({
                uri: 'pages/UserInfo',
                params: { contactData: this.contactItem }
            });
        });
        //头像
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/ContactListItem.ets(19:7)");
        //头像
        Flex.width(70);
        //头像
        Flex.height(70);
        Image.create($rawfile(this.contactItem.headImg));
        Image.debugLine("common/components/listitem/ContactListItem.ets(21:9)");
        Image.width('100%');
        Image.height('100%');
        Image.borderRadius(5);
        Image.width('70%');
        Image.height('70%');
        //头像
        Flex.pop();
        //昵称或备注
        Flex.create({ direction: FlexDirection.Column });
        Flex.debugLine("common/components/listitem/ContactListItem.ets(28:7)");
        //昵称或备注
        Flex.height(70);
        //昵称或备注
        Flex.layoutWeight(1);
        //昵称或备注
        Flex.padding({ left: 5 });
        Row.create();
        Row.debugLine("common/components/listitem/ContactListItem.ets(29:9)");
        Row.height('100%');
        Text.create(this.contactItem.mark);
        Text.debugLine("common/components/listitem/ContactListItem.ets(30:11)");
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.fontFamily({ "id": 117440976, "type": 10002, params: [] });
        Text.fontSize(20);
        Text.fontColor({ "id": 16777281, "type": 10001, params: [] });
        Text.pop();
        Row.pop();
        Divider.create();
        Divider.debugLine("common/components/listitem/ContactListItem.ets(39:9)");
        Divider.width('100%');
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        Divider.strokeWidth(0.8);
        //昵称或备注
        Flex.pop();
        Row.pop();
    }
}
exports.ContactListItem = ContactListItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/SessionListItem.ets":
/*!***********************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/SessionListItem.ets ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SessionListItem = void 0;
/**
 * 该组件为主界面第一个标签页面的item布局。
 * SessionList => item
 * powered by yiyefangzhou24
 * 2022/5/4
 */
__webpack_require__(/*! ../../../model/data/SessionData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets");
var router = globalThis.requireNativeModule('system.router');
class SessionListItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.sessionItem = undefined;
        this.__itemBgColor = new ObservedPropertyObject({ "id": 16777290, "type": 10001, params: [] }, this, "itemBgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.sessionItem !== undefined) {
            this.sessionItem = params.sessionItem;
        }
        if (params.itemBgColor !== undefined) {
            this.itemBgColor = params.itemBgColor;
        }
    }
    aboutToBeDeleted() {
        this.__itemBgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get itemBgColor() {
        return this.__itemBgColor.get();
    }
    set itemBgColor(newValue) {
        this.__itemBgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/listitem/SessionListItem.ets(16:5)");
        Row.height(78);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.itemBgColor));
        Row.onTouch((event) => {
            if (event.type === TouchType.Down) {
                this.itemBgColor = { "id": 16777287, "type": 10001, params: [] };
            }
            if (event.type === TouchType.Up) {
                this.itemBgColor = { "id": 16777290, "type": 10001, params: [] };
            }
        });
        Row.onClick(() => {
            router.push({
                uri: 'pages/Message',
                params: { sessionData: this.sessionItem }
            });
        });
        //头像
        Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
        Flex.debugLine("common/components/listitem/SessionListItem.ets(18:7)");
        //头像
        Flex.width(78);
        //头像
        Flex.height(78);
        Image.create($rawfile(this.sessionItem.headImg));
        Image.debugLine("common/components/listitem/SessionListItem.ets(20:9)");
        Image.width('100%');
        Image.height('100%');
        Image.borderRadius(5);
        Image.width('70%');
        Image.height('70%');
        //头像
        Flex.pop();
        //昵称及最后消息
        Column.create();
        Column.debugLine("common/components/listitem/SessionListItem.ets(27:7)");
        //昵称及最后消息
        Column.height(78);
        //昵称及最后消息
        Column.layoutWeight(1);
        Flex.create({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center });
        Flex.debugLine("common/components/listitem/SessionListItem.ets(28:9)");
        Flex.width('100%');
        Flex.height(77);
        Text.create(this.sessionItem.name);
        Text.debugLine("common/components/listitem/SessionListItem.ets(29:11)");
        Text.width('100%');
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.fontFamily({ "id": 117440976, "type": 10002, params: [] });
        Text.fontSize(20);
        Text.fontColor({ "id": 16777281, "type": 10001, params: [] });
        Text.margin({ bottom: 1 });
        Text.pop();
        Text.create(this.sessionItem.lastMsg);
        Text.debugLine("common/components/listitem/SessionListItem.ets(37:11)");
        Text.width('100%');
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(16);
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.margin({ top: 1 });
        Text.pop();
        Flex.pop();
        Divider.create();
        Divider.debugLine("common/components/listitem/SessionListItem.ets(46:9)");
        Divider.width('100%');
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        Divider.strokeWidth(0.8);
        //昵称及最后消息
        Column.pop();
        Row.pop();
    }
}
exports.SessionListItem = SessionListItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为导航栏，除了特定页面如闪屏页面，都应该有导航栏。
 * 导航栏固定高度60vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.navigationBar = void 0;
var router = globalThis.requireNativeModule('system.router');
class navigationBar extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__title = new ObservedPropertySimple('', this, "title");
        this.__isBack = new ObservedPropertySimple(true, this, "isBack");
        this.__isMenu = new ObservedPropertySimple(true, this, "isMenu");
        this.__isSearch = new ObservedPropertySimple(true, this, "isSearch");
        this.__isMore = new ObservedPropertySimple(false, this, "isMore");
        this.__isClose = new ObservedPropertySimple(false, this, "isClose");
        this.__bgColor = new ObservedPropertyObject({ "id": 16777285, "type": 10001, params: [] }, this, "bgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.title !== undefined) {
            this.title = params.title;
        }
        if (params.isBack !== undefined) {
            this.isBack = params.isBack;
        }
        if (params.isMenu !== undefined) {
            this.isMenu = params.isMenu;
        }
        if (params.isSearch !== undefined) {
            this.isSearch = params.isSearch;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isClose !== undefined) {
            this.isClose = params.isClose;
        }
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
    }
    aboutToBeDeleted() {
        this.__title.aboutToBeDeleted();
        this.__isBack.aboutToBeDeleted();
        this.__isMenu.aboutToBeDeleted();
        this.__isSearch.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isClose.aboutToBeDeleted();
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    get isBack() {
        return this.__isBack.get();
    }
    set isBack(newValue) {
        this.__isBack.set(newValue);
    }
    get isMenu() {
        return this.__isMenu.get();
    }
    set isMenu(newValue) {
        this.__isMenu.set(newValue);
    }
    get isSearch() {
        return this.__isSearch.get();
    }
    set isSearch(newValue) {
        this.__isSearch.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isClose() {
        return this.__isClose.get();
    }
    set isClose(newValue) {
        this.__isClose.set(newValue);
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/navigationBar.ets(21:4)");
        Row.height(60);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(26:7)");
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isBack == true) {
            If.branchId(0);
            Image.create({ "id": 16777296, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(28:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        If.create();
        if (this.isClose == true) {
            If.branchId(0);
            Image.create({ "id": 16777299, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(36:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.pop();
        /**
         * 中间标题栏
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(47:7)");
        /**
         * 中间标题栏
         */
        Column.layoutWeight(1);
        /**
         * 中间标题栏
         */
        Column.padding({ left: 20 });
        Text.create(this.title);
        Text.debugLine("common/components/navigationBar.ets(48:9)");
        Text.fontSize(18);
        Text.fontFamily({ "id": 117440975, "type": 10002, params: [] });
        Text.maxLines(1);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.pop();
        /**
         * 中间标题栏
         */
        Column.pop();
        /**
         * 右侧搜索按钮
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(57:7)");
        /**
         * 右侧搜索按钮
         */
        Column.width(30);
        If.create();
        if (this.isSearch == true) {
            If.branchId(0);
            Image.create({ "id": 16777328, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(59:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧搜索按钮
         */
        Column.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(68:7)");
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isMenu == true) {
            If.branchId(0);
            Image.create({ "id": 16777295, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(70:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        If.create();
        if (this.isMore == true) {
            If.branchId(0);
            Image.create({ "id": 16777322, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(75:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.pop();
        Row.pop();
    }
}
exports.navigationBar = navigationBar;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/ContactTabContent.ets":
/*!***************************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/ContactTabContent.ets ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ContactTabContent = void 0;
/**
 * 该组件为主界面第二个标签页面。
 * 该标签页显示联系人信息
 * powered by yiyefangzhou24
 * 2022/5/14
 */
const ContactData_ets_1 = __webpack_require__(/*! ../../../model/data/ContactData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets");
const ContactModel_ets_1 = __webpack_require__(/*! ../../../model/ContactModel.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/ContactModel.ets");
const ContactListItem_ets_1 = __webpack_require__(/*! ../listitem/ContactListItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/ContactListItem.ets");
class ContactTabContent extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.value = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
        this.contactItems = ContactModel_ets_1.getContactDatas() //获取联系人数据
        ;
        this.systemItems = [
            {
                "wid": "0",
                "headImg": "ic_new_friend.png",
                "name": "新的朋友",
                "mark": "新的朋友",
                "gender": ContactData_ets_1.Gender.male,
                "area": ""
            },
            {
                "wid": "1",
                "headImg": "ic_group_cheat.png",
                "name": "群聊",
                "mark": "群聊",
                "gender": ContactData_ets_1.Gender.male,
                "area": ""
            },
            {
                "wid": "2",
                "headImg": "ic_tag.png",
                "name": "标签",
                "mark": "标签",
                "gender": ContactData_ets_1.Gender.male,
                "area": ""
            },
            {
                "wid": "3",
                "headImg": "ic_offical.png",
                "name": "公众号",
                "mark": "公众号",
                "gender": ContactData_ets_1.Gender.male,
                "area": ""
            }
        ];
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.value !== undefined) {
            this.value = params.value;
        }
        if (params.contactItems !== undefined) {
            this.contactItems = params.contactItems;
        }
        if (params.systemItems !== undefined) {
            this.systemItems = params.systemItems;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Stack.create();
        Stack.debugLine("common/components/tabcontent/ContactTabContent.ets(50:5)");
        Column.create();
        Column.debugLine("common/components/tabcontent/ContactTabContent.ets(51:7)");
        Column.width('100%');
        Column.height('100%');
        Column.backgroundColor({ "id": 16777287, "type": 10001, params: [] });
        /**
         * 顶部系统选项
         * 警告：每一个item的高度预设为70，如果改变高度，这里要手动重新计算系统选项高度
         */
        List.create();
        List.debugLine("common/components/tabcontent/ContactTabContent.ets(56:9)");
        /**
         * 顶部系统选项
         * 警告：每一个item的高度预设为70，如果改变高度，这里要手动重新计算系统选项高度
         */
        List.width('100%');
        /**
         * 顶部系统选项
         * 警告：每一个item的高度预设为70，如果改变高度，这里要手动重新计算系统选项高度
         */
        List.height(this.systemItems.length * 70);
        ForEach.create("2", this, ObservedObject.GetRawObject(this.systemItems), item => {
            ListItem.create();
            ListItem.debugLine("common/components/tabcontent/ContactTabContent.ets(58:13)");
            let earlierCreatedChild_1 = this.findChildById("1");
            if (earlierCreatedChild_1 == undefined) {
                View.create(new ContactListItem_ets_1.ContactListItem("1", this, { contactItem: item }));
            }
            else {
                earlierCreatedChild_1.updateWithValueParams({
                    contactItem: item
                });
                View.create(earlierCreatedChild_1);
            }
            ListItem.pop();
        }, item => item.wid);
        ForEach.pop();
        /**
         * 顶部系统选项
         * 警告：每一个item的高度预设为70，如果改变高度，这里要手动重新计算系统选项高度
         */
        List.pop();
        /**
         * 联系人列表
         */
        List.create();
        List.debugLine("common/components/tabcontent/ContactTabContent.ets(68:9)");
        /**
         * 联系人列表
         */
        List.width('100%');
        /**
         * 联系人列表
         */
        List.margin({ top: 10 });
        /**
         * 联系人列表
         */
        List.layoutWeight(1);
        ForEach.create("4", this, ObservedObject.GetRawObject(this.contactItems), item => {
            ListItem.create();
            ListItem.debugLine("common/components/tabcontent/ContactTabContent.ets(70:13)");
            let earlierCreatedChild_3 = this.findChildById("3");
            if (earlierCreatedChild_3 == undefined) {
                View.create(new ContactListItem_ets_1.ContactListItem("3", this, { contactItem: item }));
            }
            else {
                earlierCreatedChild_3.updateWithValueParams({
                    contactItem: item
                });
                View.create(earlierCreatedChild_3);
            }
            ListItem.pop();
        }, item => item.wid);
        ForEach.pop();
        /**
         * 联系人列表
         */
        List.pop();
        Column.pop();
        Stack.pop();
    }
}
exports.ContactTabContent = ContactTabContent;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/DiscoveryTabContent.ets":
/*!*****************************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/DiscoveryTabContent.ets ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DiscoveryTabContent = void 0;
/**
 * 该组件为主界面第三个标签页面。
 * 该标签页显示发现页面信息
 * powered by yiyefangzhou24
 * 2022/5/14
 */
const optionItem_ets_1 = __webpack_require__(/*! ../../utils/optionItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets");
var router = globalThis.requireNativeModule('system.router');
class DiscoveryTabContent extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.scroller = new Scroller();
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.scroller !== undefined) {
            this.scroller = params.scroller;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    momentItemOnClick(event) {
        router.push({
            uri: 'pages/Moments'
        });
    }
    render() {
        /**
         * 功能列表
         * 注：这里加了一个Scroll组件，防止有更多的optionItem，自动开启向下滑动
         */
        Column.create();
        Column.debugLine("common/components/tabcontent/DiscoveryTabContent.ets(24:5)");
        /**
         * 功能列表
         * 注：这里加了一个Scroll组件，防止有更多的optionItem，自动开启向下滑动
         */
        Column.width('100%');
        /**
         * 功能列表
         * 注：这里加了一个Scroll组件，防止有更多的optionItem，自动开启向下滑动
         */
        Column.height('100%');
        /**
         * 功能列表
         * 注：这里加了一个Scroll组件，防止有更多的optionItem，自动开启向下滑动
         */
        Column.backgroundColor({ "id": 16777287, "type": 10001, params: [] });
        Scroll.create(this.scroller);
        Scroll.debugLine("common/components/tabcontent/DiscoveryTabContent.ets(25:7)");
        Scroll.scrollable(ScrollDirection.Vertical);
        Scroll.scrollBar(BarState.Off);
        Column.create();
        Column.debugLine("common/components/tabcontent/DiscoveryTabContent.ets(26:9)");
        Column.width('100%');
        let earlierCreatedChild_1 = this.findChildById("1");
        if (earlierCreatedChild_1 == undefined) {
            //朋友圈
            View.create(new optionItem_ets_1.optionItem("1", this, {
                img: { "id": 16777321, "type": 20000, params: [] },
                context: { "id": 16777220, "type": 10003, params: [] },
                isNext: true,
                marginTop: 0,
                itemOnClick: this.momentItemOnClick
            }));
        }
        else {
            earlierCreatedChild_1.updateWithValueParams({
                img: { "id": 16777321, "type": 20000, params: [] },
                context: { "id": 16777220, "type": 10003, params: [] },
                isNext: true,
                marginTop: 0,
                itemOnClick: this.momentItemOnClick
            });
            View.create(earlierCreatedChild_1);
        }
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            //视频号
            View.create(new optionItem_ets_1.optionItem("2", this, {
                img: { "id": 16777335, "type": 20000, params: [] },
                context: { "id": 16777224, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                img: { "id": 16777335, "type": 20000, params: [] },
                context: { "id": 16777224, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            });
            View.create(earlierCreatedChild_2);
        }
        let earlierCreatedChild_3 = this.findChildById("3");
        if (earlierCreatedChild_3 == undefined) {
            //扫一扫
            View.create(new optionItem_ets_1.optionItem("3", this, { img: { "id": 16777327, "type": 20000, params: [] }, context: { "id": 16777223, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_3.updateWithValueParams({
                img: { "id": 16777327, "type": 20000, params: [] }, context: { "id": 16777223, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_3);
        }
        let earlierCreatedChild_4 = this.findChildById("4");
        if (earlierCreatedChild_4 == undefined) {
            //附近
            View.create(new optionItem_ets_1.optionItem("4", this, {
                img: { "id": 16777323, "type": 20000, params: [] },
                context: { "id": 16777221, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            }));
        }
        else {
            earlierCreatedChild_4.updateWithValueParams({
                img: { "id": 16777323, "type": 20000, params: [] },
                context: { "id": 16777221, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            });
            View.create(earlierCreatedChild_4);
        }
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            //小程序
            View.create(new optionItem_ets_1.optionItem("5", this, {
                img: { "id": 16777325, "type": 20000, params: [] },
                context: { "id": 16777222, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            }));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({
                img: { "id": 16777325, "type": 20000, params: [] },
                context: { "id": 16777222, "type": 10003, params: [] },
                isNext: true,
                marginTop: 10
            });
            View.create(earlierCreatedChild_5);
        }
        Column.pop();
        Scroll.pop();
        /**
         * 功能列表
         * 注：这里加了一个Scroll组件，防止有更多的optionItem，自动开启向下滑动
         */
        Column.pop();
    }
}
exports.DiscoveryTabContent = DiscoveryTabContent;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/MeTabContent.ets":
/*!**********************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/MeTabContent.ets ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MeTabContent = void 0;
/**
 * 该组件为主界面第四个标签页面。
 * 该标签页显示个人设定信息界面
 * powered by yiyefangzhou24
 * 2022/5/15
 */
const optionItem_ets_1 = __webpack_require__(/*! ../../utils/optionItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets");
var router = globalThis.requireNativeModule('system.router');
class MeTabContent extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.headerImg = 'header1.png';
        this.name = '刘备';
        this.id = '10000';
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.headerImg !== undefined) {
            this.headerImg = params.headerImg;
        }
        if (params.name !== undefined) {
            this.name = params.name;
        }
        if (params.id !== undefined) {
            this.id = params.id;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    settingItemOnClick(event) {
        router.push({
            uri: 'pages/Setting'
        });
    }
    render() {
        Column.create();
        Column.debugLine("common/components/tabcontent/MeTabContent.ets(22:5)");
        Column.width('100%');
        Column.height('100%');
        Column.backgroundColor({ "id": 16777287, "type": 10001, params: [] });
        /**
         * 顶端的头像和昵称按钮
         * 包括：头像+昵称+微信ID
         */
        Row.create();
        Row.debugLine("common/components/tabcontent/MeTabContent.ets(27:7)");
        /**
         * 顶端的头像和昵称按钮
         * 包括：头像+昵称+微信ID
         */
        Row.width('100%');
        /**
         * 顶端的头像和昵称按钮
         * 包括：头像+昵称+微信ID
         */
        Row.height(180);
        /**
         * 顶端的头像和昵称按钮
         * 包括：头像+昵称+微信ID
         */
        Row.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Image.create($rawfile(this.headerImg));
        Image.debugLine("common/components/tabcontent/MeTabContent.ets(28:9)");
        Image.width(80);
        Image.height(80);
        Image.borderRadius(8);
        Image.margin(20);
        Column.create();
        Column.debugLine("common/components/tabcontent/MeTabContent.ets(33:9)");
        Column.alignItems(HorizontalAlign.Start);
        Column.layoutWeight(1);
        Text.create(this.name);
        Text.debugLine("common/components/tabcontent/MeTabContent.ets(34:11)");
        Text.fontFamily({ "id": 117440974, "type": 10002, params: [] });
        Text.fontSize(28);
        Text.textOverflow({ overflow: TextOverflow.Clip });
        Text.maxLines(1);
        Text.fontWeight(FontWeight.Bold);
        Text.margin({ bottom: 10 });
        Text.pop();
        Text.create();
        Text.debugLine("common/components/tabcontent/MeTabContent.ets(41:11)");
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Span.create({ "id": 16777252, "type": 10003, params: [] });
        Span.debugLine("common/components/tabcontent/MeTabContent.ets(42:13)");
        Span.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Span.fontSize(18);
        Span.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Span.create(this.id);
        Span.debugLine("common/components/tabcontent/MeTabContent.ets(46:13)");
        Span.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Span.fontSize(18);
        Span.fontColor({ "id": 16777288, "type": 10001, params: [] });
        Text.pop();
        Column.pop();
        Image.create({ "id": 16777326, "type": 20000, params: [] });
        Image.debugLine("common/components/tabcontent/MeTabContent.ets(56:9)");
        Image.width(18);
        Image.height(18);
        Image.margin(10);
        Image.create({ "id": 16777324, "type": 20000, params: [] });
        Image.debugLine("common/components/tabcontent/MeTabContent.ets(60:9)");
        Image.width(18);
        Image.height(18);
        Image.margin(10);
        /**
         * 顶端的头像和昵称按钮
         * 包括：头像+昵称+微信ID
         */
        Row.pop();
        /**
         * 功能列表
         */
        Column.create();
        Column.debugLine("common/components/tabcontent/MeTabContent.ets(71:7)");
        let earlierCreatedChild_1 = this.findChildById("1");
        if (earlierCreatedChild_1 == undefined) {
            //服务
            View.create(new optionItem_ets_1.optionItem("1", this, { img: { "id": 16777318, "type": 20000, params: [] }, context: { "id": 16777253, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_1.updateWithValueParams({
                img: { "id": 16777318, "type": 20000, params: [] }, context: { "id": 16777253, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_1);
        }
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            //收藏
            View.create(new optionItem_ets_1.optionItem("2", this, { img: { "id": 16777315, "type": 20000, params: [] }, context: { "id": 16777249, "type": 10003, params: [] }, isNext: true, marginTop: 10 }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                img: { "id": 16777315, "type": 20000, params: [] }, context: { "id": 16777249, "type": 10003, params: [] },
                isNext: true, marginTop: 10
            });
            View.create(earlierCreatedChild_2);
        }
        let earlierCreatedChild_3 = this.findChildById("3");
        if (earlierCreatedChild_3 == undefined) {
            //朋友圈
            View.create(new optionItem_ets_1.optionItem("3", this, { img: { "id": 16777317, "type": 20000, params: [] }, context: { "id": 16777251, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_3.updateWithValueParams({
                img: { "id": 16777317, "type": 20000, params: [] }, context: { "id": 16777251, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_3);
        }
        let earlierCreatedChild_4 = this.findChildById("4");
        if (earlierCreatedChild_4 == undefined) {
            //卡包
            View.create(new optionItem_ets_1.optionItem("4", this, { img: { "id": 16777314, "type": 20000, params: [] }, context: { "id": 16777248, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_4.updateWithValueParams({
                img: { "id": 16777314, "type": 20000, params: [] }, context: { "id": 16777248, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_4);
        }
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            //表情
            View.create(new optionItem_ets_1.optionItem("5", this, { img: { "id": 16777316, "type": 20000, params: [] }, context: { "id": 16777250, "type": 10003, params: [] }, isNext: true, marginTop: 0 }));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({
                img: { "id": 16777316, "type": 20000, params: [] }, context: { "id": 16777250, "type": 10003, params: [] },
                isNext: true, marginTop: 0
            });
            View.create(earlierCreatedChild_5);
        }
        let earlierCreatedChild_6 = this.findChildById("6");
        if (earlierCreatedChild_6 == undefined) {
            //设置
            View.create(new optionItem_ets_1.optionItem("6", this, { img: { "id": 16777319, "type": 20000, params: [] }, context: { "id": 16777254, "type": 10003, params: [] }, isNext: true, marginTop: 10, itemOnClick: this.settingItemOnClick }));
        }
        else {
            earlierCreatedChild_6.updateWithValueParams({
                img: { "id": 16777319, "type": 20000, params: [] }, context: { "id": 16777254, "type": 10003, params: [] },
                isNext: true, marginTop: 10, itemOnClick: this.settingItemOnClick
            });
            View.create(earlierCreatedChild_6);
        }
        /**
         * 功能列表
         */
        Column.pop();
        Column.pop();
    }
}
exports.MeTabContent = MeTabContent;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/SessionTabContent.ets":
/*!***************************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/SessionTabContent.ets ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 该组件为主界面第一个标签页面。
 * 该标签页显示最近会画信息
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SessionTabContent = void 0;
__webpack_require__(/*! ../../../../default/model/data/SessionData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets");
const SessionModel_ets_1 = __webpack_require__(/*! ../../../../default/model/SessionModel.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/SessionModel.ets");
const SessionListItem_ets_1 = __webpack_require__(/*! ../../components/listitem/SessionListItem.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/listitem/SessionListItem.ets");
class SessionTabContent extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.sessionItem = SessionModel_ets_1.getSessionDatas();
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.sessionItem !== undefined) {
            this.sessionItem = params.sessionItem;
        }
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Column.create();
        Column.debugLine("common/components/tabcontent/SessionTabContent.ets(17:5)");
        Column.width('100%');
        Column.height('100%');
        Column.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        /**
         * 聊天会话列表
         */
        List.create();
        List.debugLine("common/components/tabcontent/SessionTabContent.ets(21:7)");
        ForEach.create("2", this, ObservedObject.GetRawObject(this.sessionItem), item => {
            ListItem.create();
            ListItem.debugLine("common/components/tabcontent/SessionTabContent.ets(23:11)");
            let earlierCreatedChild_1 = this.findChildById("1");
            if (earlierCreatedChild_1 == undefined) {
                View.create(new SessionListItem_ets_1.SessionListItem("1", this, { sessionItem: item }));
            }
            else {
                earlierCreatedChild_1.updateWithValueParams({
                    sessionItem: item
                });
                View.create(earlierCreatedChild_1);
            }
            ListItem.pop();
        }, item => item.wid);
        ForEach.pop();
        /**
         * 聊天会话列表
         */
        List.pop();
        Column.pop();
    }
}
exports.SessionTabContent = SessionTabContent;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets":
/*!****************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/utils/optionItem.ets ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为设置和其他类似UI的选项布局。
 * 主要内容为图片+文字+导航箭头
 * powered by yiyefangzhou24
 * 2022/5/15
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.optionItem = void 0;
class optionItem extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__bgColor = new ObservedPropertyObject({ "id": 16777290, "type": 10001, params: [] }, this, "bgColor");
        this.img = undefined;
        this.context = undefined;
        this.isNext = undefined;
        this.marginTop = undefined;
        this.itemOnClick = (event) => { } //item的单击事件（可选）
        ;
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
        if (params.img !== undefined) {
            this.img = params.img;
        }
        if (params.context !== undefined) {
            this.context = params.context;
        }
        if (params.isNext !== undefined) {
            this.isNext = params.isNext;
        }
        if (params.marginTop !== undefined) {
            this.marginTop = params.marginTop;
        }
        if (params.itemOnClick !== undefined) {
            this.itemOnClick = params.itemOnClick;
        }
    }
    aboutToBeDeleted() {
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(19:5)");
        Row.width('100%');
        Row.height(60);
        Row.margin({ top: this.marginTop });
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        Row.onTouch((event) => {
            if (event.type === TouchType.Down) {
                this.bgColor = { "id": 16777287, "type": 10001, params: [] };
            }
            if (event.type === TouchType.Up) {
                this.bgColor = { "id": 16777290, "type": 10001, params: [] };
            }
        });
        Row.onClick((event) => {
            this.itemOnClick(event);
        });
        If.create();
        if (this.img != null) {
            If.branchId(0);
            Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center });
            Flex.debugLine("common/utils/optionItem.ets(21:9)");
            Flex.width(50);
            Flex.height(60);
            Image.create(this.img);
            Image.debugLine("common/utils/optionItem.ets(22:11)");
            Image.width(30);
            Image.height(30);
            Flex.pop();
        }
        If.pop();
        Column.create();
        Column.debugLine("common/utils/optionItem.ets(26:7)");
        Column.padding({ left: 10, right: 4 });
        Column.height(60);
        Column.layoutWeight(1);
        Row.create();
        Row.debugLine("common/utils/optionItem.ets(27:9)");
        Row.height(59);
        Text.create(this.context);
        Text.debugLine("common/utils/optionItem.ets(28:11)");
        Text.fontFamily({ "id": 117440980, "type": 10002, params: [] });
        Text.fontSize(19);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.maxLines(1);
        Text.layoutWeight(1);
        Text.pop();
        If.create();
        //.margin({left: 10})
        if (this.isNext == true) {
            If.branchId(0);
            Image.create({ "id": 16777324, "type": 20000, params: [] });
            Image.debugLine("common/utils/optionItem.ets(36:13)");
            Image.width(18);
            Image.height(18);
            Image.margin(10);
        }
        If.pop();
        Row.pop();
        Divider.create();
        Divider.debugLine("common/utils/optionItem.ets(42:9)");
        Divider.color({ "id": 16777287, "type": 10001, params: [] });
        Divider.strokeWidth(0.8);
        Column.pop();
        Row.pop();
    }
}
exports.optionItem = optionItem;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/ContactModel.ets":
/*!***********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/ContactModel.ets ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getContactDatas = void 0;
const ContactData_ets_1 = __webpack_require__(/*! ./data/ContactData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets");
/**
 * 用于初始化ContactData的数组
 */
function getContactDatas() {
    let contactDataArray = [];
    ContactsComposition.forEach(item => {
        contactDataArray.push(new ContactData_ets_1.ContactData(item.wid, item.headImg, item.name, item.mark, item.gender, item.area));
    });
    return contactDataArray;
}
exports.getContactDatas = getContactDatas;
/**
 * 定义contact数据测试样例
 */
const ContactsComposition = [
    {
        "wid": "10000",
        "headImg": "header1.png",
        "name": "刘备",
        "mark": "刘备",
        "gender": ContactData_ets_1.Gender.male,
        "area": "三国·蜀国"
    }, {
        "wid": "10001",
        "headImg": "header2.png",
        "name": "诸葛孔明",
        "mark": "诸葛亮",
        "gender": ContactData_ets_1.Gender.male,
        "area": "三国·蜀国"
    },
    {
        "wid": "10002",
        "headImg": "header3.png",
        "name": "关羽",
        "mark": "关羽",
        "gender": ContactData_ets_1.Gender.male,
        "area": "三国·蜀国"
    },
    {
        "wid": "10003",
        "headImg": "header4.png",
        "name": "张飞",
        "mark": "张飞",
        "gender": ContactData_ets_1.Gender.male,
        "area": "三国·蜀国"
    }
];


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/SessionModel.ets":
/*!***********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/SessionModel.ets ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


/**
 * 定义主界面最近会话界面的数据样例
 * 该模型与ability对接
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getSessionDatas = void 0;
const SessionData_ets_1 = __webpack_require__(/*! ./data/SessionData.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets");
/**
 * 用于初始化SessionData的数组
 */
function getSessionDatas() {
    let sessionsDataArray = [];
    SessionsComposition.forEach(item => {
        sessionsDataArray.push(new SessionData_ets_1.SessionData(item.wid, item.headImg, item.name, item.lastMsg, item.unReadmsg));
    });
    sessionsDataArray.forEach(item => {
        console.log('wid:' + item.wid +
            '|header:' + item.headImg +
            '|name:' + item.name +
            '|lastMsg:' + item.lastMsg +
            '|unread:' + item.unReadMsg.toString());
    });
    return sessionsDataArray;
}
exports.getSessionDatas = getSessionDatas;
/**
 * 定义会话列表数据测试样例
 */
const SessionsComposition = [
    {
        "wid": "10000",
        "headImg": "header1.png",
        "name": "刘备",
        "lastMsg": "天下碌碌之辈,诚不足虑也。",
        "unReadmsg": 10
    },
    {
        "wid": "10001",
        "headImg": "header2.png",
        "name": "诸葛亮",
        "lastMsg": "愿陛下托臣以讨贼兴复之效，不效，则治臣之罪，以告先帝之灵。若无兴德之言，则责攸之、祎、允等之慢，以彰其咎；陛下亦宜自谋，以咨诹善道，察纳雅言。深追先帝遗诏，臣不胜受恩感激。",
        "unReadmsg": 0
    },
    {
        "wid": "10002",
        "headImg": "header3.png",
        "name": "关羽",
        "lastMsg": "玉可碎不可改其质，竹可焚不可毁其节。",
        "unReadmsg": 0
    },
    {
        "wid": "10003",
        "headImg": "header4.png",
        "name": "张飞",
        "lastMsg": "[图片]",
        "unReadmsg": 0
    }
];


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets":
/*!***************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/ContactData.ets ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 联系人数据结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ContactData = exports.Gender = void 0;
var Gender;
(function (Gender) {
    //女性
    Gender[Gender["female"] = 0] = "female";
    //男性
    Gender[Gender["male"] = 1] = "male";
})(Gender = exports.Gender || (exports.Gender = {}));
class ContactData {
    constructor(wid, headImg, name, mark, gender, area) {
        this.wid = wid;
        this.headImg = headImg;
        this.name = name;
        this.mark = mark;
        this.gender = gender;
        this.area = area;
    }
}
exports.ContactData = ContactData;


/***/ }),

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets":
/*!***************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/model/data/SessionData.ets ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 主界面会话列表的数据结构
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SessionData = void 0;
class SessionData {
    constructor(wid, headImg, name, lastMsg, unReadMsg) {
        this.wid = wid;
        this.headImg = headImg;
        this.name = name;
        this.lastMsg = lastMsg;
        this.unReadMsg = unReadMsg;
    }
}
exports.SessionData = SessionData;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!*********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Main.ets?entry ***!
  \*********************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为主界面。
 * 包含：微信/通讯录/发现/我 四个tab页面
 * powered by yiyefangzhou24
 * 2022/5/14
 */
const navigationBar_ets_1 = __webpack_require__(/*! ../common/components/navigationBar.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets");
const bottomTabs_1 = __webpack_require__(/*! ../common/components/bottomTabs */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/bottomTabs.ets");
const SessionTabContent_ets_1 = __webpack_require__(/*! ../common/components/tabcontent/SessionTabContent.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/SessionTabContent.ets");
const ContactTabContent_ets_1 = __webpack_require__(/*! ../common/components/tabcontent/ContactTabContent.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/ContactTabContent.ets");
const MeTabContent_ets_1 = __webpack_require__(/*! ../common/components/tabcontent/MeTabContent.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/MeTabContent.ets");
const DiscoveryTabContent_ets_1 = __webpack_require__(/*! ../common/components/tabcontent/DiscoveryTabContent.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/tabcontent/DiscoveryTabContent.ets");
class Main extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.controller = new TabsController();
        this.__bottomTabIndex = new ObservedPropertySimple(0, this, "bottomTabIndex");
        this.__title = new ObservedPropertyObject([{ "id": 16777280, "type": 10003, params: [] }, { "id": 16777278, "type": 10003, params: [] }, { "id": 16777279, "type": 10003, params: [] }], this, "title");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.controller !== undefined) {
            this.controller = params.controller;
        }
        if (params.bottomTabIndex !== undefined) {
            this.bottomTabIndex = params.bottomTabIndex;
        }
        if (params.title !== undefined) {
            this.title = params.title;
        }
    }
    aboutToBeDeleted() {
        this.__bottomTabIndex.aboutToBeDeleted();
        this.__title.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get bottomTabIndex() {
        return this.__bottomTabIndex.get();
    }
    set bottomTabIndex(newValue) {
        this.__bottomTabIndex.set(newValue);
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    render() {
        Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.End, justifyContent: FlexAlign.End });
        Flex.debugLine("pages/Main.ets(23:5)");
        Flex.backgroundColor({ "id": 16777290, "type": 10001, params: [] });
        Flex.width('100%');
        Flex.height('100%');
        If.create();
        /**
         * 导航栏
         * 微信/通讯录/发现需要导航栏，个人设定界面不需要导航栏
         * 详见naigationBar.ets
         */
        if (this.bottomTabIndex < 3) {
            If.branchId(0);
            let earlierCreatedChild_2 = this.findChildById("2");
            if (earlierCreatedChild_2 == undefined) {
                View.create(new navigationBar_ets_1.navigationBar("2", this, { title: this.title[this.bottomTabIndex], isBack: false, isClose: false, isMenu: true, isSearch: true, isMore: false }));
            }
            else {
                earlierCreatedChild_2.updateWithValueParams({
                    title: this.title[this.bottomTabIndex], isBack: false, isClose: false, isMenu: true, isSearch: true, isMore: false
                });
                View.create(earlierCreatedChild_2);
            }
        }
        If.pop();
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.create({ barPosition: BarPosition.End, index: 0, controller: this.controller });
        Tabs.debugLine("pages/Main.ets(36:7)");
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.onChange((index) => {
            this.bottomTabIndex = index;
        });
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.vertical(false);
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.barHeight(0);
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.width('100%');
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.scrollable(true);
        TabContent.create();
        TabContent.debugLine("pages/Main.ets(37:9)");
        let earlierCreatedChild_3 = this.findChildById("3");
        if (earlierCreatedChild_3 == undefined) {
            View.create(new SessionTabContent_ets_1.SessionTabContent("3", this, {}));
        }
        else {
            earlierCreatedChild_3.updateWithValueParams({});
            if (!earlierCreatedChild_3.needsUpdate()) {
                earlierCreatedChild_3.markStatic();
            }
            View.create(earlierCreatedChild_3);
        }
        TabContent.pop();
        TabContent.create();
        TabContent.debugLine("pages/Main.ets(40:9)");
        let earlierCreatedChild_4 = this.findChildById("4");
        if (earlierCreatedChild_4 == undefined) {
            View.create(new ContactTabContent_ets_1.ContactTabContent("4", this, {}));
        }
        else {
            earlierCreatedChild_4.updateWithValueParams({});
            if (!earlierCreatedChild_4.needsUpdate()) {
                earlierCreatedChild_4.markStatic();
            }
            View.create(earlierCreatedChild_4);
        }
        TabContent.pop();
        TabContent.create();
        TabContent.debugLine("pages/Main.ets(43:9)");
        let earlierCreatedChild_5 = this.findChildById("5");
        if (earlierCreatedChild_5 == undefined) {
            View.create(new DiscoveryTabContent_ets_1.DiscoveryTabContent("5", this, {}));
        }
        else {
            earlierCreatedChild_5.updateWithValueParams({});
            if (!earlierCreatedChild_5.needsUpdate()) {
                earlierCreatedChild_5.markStatic();
            }
            View.create(earlierCreatedChild_5);
        }
        TabContent.pop();
        TabContent.create();
        TabContent.debugLine("pages/Main.ets(46:9)");
        let earlierCreatedChild_6 = this.findChildById("6");
        if (earlierCreatedChild_6 == undefined) {
            View.create(new MeTabContent_ets_1.MeTabContent("6", this, {}));
        }
        else {
            earlierCreatedChild_6.updateWithValueParams({});
            if (!earlierCreatedChild_6.needsUpdate()) {
                earlierCreatedChild_6.markStatic();
            }
            View.create(earlierCreatedChild_6);
        }
        TabContent.pop();
        /**
         * 中间的标签页组件
         * 分别为 最近会话/联系人/设置
         */
        Tabs.pop();
        let earlierCreatedChild_7 = this.findChildById("7");
        if (earlierCreatedChild_7 == undefined) {
            /**
             * 底部tab标签页
             * 详见bottomTabs.ets
             */
            View.create(new bottomTabs_1.BottomTabs("7", this, { controller: this.controller, bottomTabIndex: this.__bottomTabIndex }));
        }
        else {
            earlierCreatedChild_7.updateWithValueParams({
                controller: this.controller
            });
            View.create(earlierCreatedChild_7);
        }
        Flex.pop();
    }
}
loadDocument(new Main("1", undefined, {}));

})();

/******/ })()
;