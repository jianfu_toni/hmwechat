/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets":
/*!************************************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


/**
 * 该组件为导航栏，除了特定页面如闪屏页面，都应该有导航栏。
 * 导航栏固定高度60vp，宽度为100%
 * powered by yiyefangzhou24
 * 2022/5/4
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.navigationBar = void 0;
var router = globalThis.requireNativeModule('system.router');
class navigationBar extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.__title = new ObservedPropertySimple('', this, "title");
        this.__isBack = new ObservedPropertySimple(true, this, "isBack");
        this.__isMenu = new ObservedPropertySimple(true, this, "isMenu");
        this.__isSearch = new ObservedPropertySimple(true, this, "isSearch");
        this.__isMore = new ObservedPropertySimple(false, this, "isMore");
        this.__isClose = new ObservedPropertySimple(false, this, "isClose");
        this.__bgColor = new ObservedPropertyObject({ "id": 16777285, "type": 10001, params: [] }, this, "bgColor");
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
        if (params.title !== undefined) {
            this.title = params.title;
        }
        if (params.isBack !== undefined) {
            this.isBack = params.isBack;
        }
        if (params.isMenu !== undefined) {
            this.isMenu = params.isMenu;
        }
        if (params.isSearch !== undefined) {
            this.isSearch = params.isSearch;
        }
        if (params.isMore !== undefined) {
            this.isMore = params.isMore;
        }
        if (params.isClose !== undefined) {
            this.isClose = params.isClose;
        }
        if (params.bgColor !== undefined) {
            this.bgColor = params.bgColor;
        }
    }
    aboutToBeDeleted() {
        this.__title.aboutToBeDeleted();
        this.__isBack.aboutToBeDeleted();
        this.__isMenu.aboutToBeDeleted();
        this.__isSearch.aboutToBeDeleted();
        this.__isMore.aboutToBeDeleted();
        this.__isClose.aboutToBeDeleted();
        this.__bgColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id());
    }
    get title() {
        return this.__title.get();
    }
    set title(newValue) {
        this.__title.set(newValue);
    }
    get isBack() {
        return this.__isBack.get();
    }
    set isBack(newValue) {
        this.__isBack.set(newValue);
    }
    get isMenu() {
        return this.__isMenu.get();
    }
    set isMenu(newValue) {
        this.__isMenu.set(newValue);
    }
    get isSearch() {
        return this.__isSearch.get();
    }
    set isSearch(newValue) {
        this.__isSearch.set(newValue);
    }
    get isMore() {
        return this.__isMore.get();
    }
    set isMore(newValue) {
        this.__isMore.set(newValue);
    }
    get isClose() {
        return this.__isClose.get();
    }
    set isClose(newValue) {
        this.__isClose.set(newValue);
    }
    get bgColor() {
        return this.__bgColor.get();
    }
    set bgColor(newValue) {
        this.__bgColor.set(newValue);
    }
    render() {
        Row.create();
        Row.debugLine("common/components/navigationBar.ets(21:4)");
        Row.height(60);
        Row.width('100%');
        Row.backgroundColor(ObservedObject.GetRawObject(this.bgColor));
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(26:7)");
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isBack == true) {
            If.branchId(0);
            Image.create({ "id": 16777296, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(28:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        If.create();
        if (this.isClose == true) {
            If.branchId(0);
            Image.create({ "id": 16777299, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(36:11)");
            Image.width(26);
            Image.height(26);
            Image.onClick(() => {
                router.back();
            });
        }
        If.pop();
        /**
         * 左侧的 后退/关闭 按钮
         * isBack和isClose变量为互斥关系，不能共存
         */
        Column.pop();
        /**
         * 中间标题栏
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(47:7)");
        /**
         * 中间标题栏
         */
        Column.layoutWeight(1);
        /**
         * 中间标题栏
         */
        Column.padding({ left: 20 });
        Text.create(this.title);
        Text.debugLine("common/components/navigationBar.ets(48:9)");
        Text.fontSize(18);
        Text.fontFamily({ "id": 117440975, "type": 10002, params: [] });
        Text.maxLines(1);
        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
        Text.pop();
        /**
         * 中间标题栏
         */
        Column.pop();
        /**
         * 右侧搜索按钮
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(57:7)");
        /**
         * 右侧搜索按钮
         */
        Column.width(30);
        If.create();
        if (this.isSearch == true) {
            If.branchId(0);
            Image.create({ "id": 16777328, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(59:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧搜索按钮
         */
        Column.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.create();
        Column.debugLine("common/components/navigationBar.ets(68:7)");
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.width(50);
        If.create();
        if (this.isMenu == true) {
            If.branchId(0);
            Image.create({ "id": 16777295, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(70:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        If.create();
        if (this.isMore == true) {
            If.branchId(0);
            Image.create({ "id": 16777322, "type": 20000, params: [] });
            Image.debugLine("common/components/navigationBar.ets(75:11)");
            Image.width(26);
            Image.height(26);
        }
        If.pop();
        /**
         * 右侧菜单按钮
         * isMenu和isMore变量为互斥关系，不能共存
         */
        Column.pop();
        Row.pop();
    }
}
exports.navigationBar = navigationBar;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!**********************************************************************************************!*\
  !*** ../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/pages/Login.ets?entry ***!
  \**********************************************************************************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
/**
 * 该组件为登陆页面。
 * powered by yiyefangzhou24
 * 2022/5/14
 */
const navigationBar_ets_1 = __webpack_require__(/*! ../common/components/navigationBar.ets */ "../../../../../../../Desktop/HMWeChat/entry/src/main/ets/default/common/components/navigationBar.ets");
var router = globalThis.requireNativeModule('system.router');
function __Text__text_style() {
    Text.width('25%');
    Text.fontFamily({ "id": 117440981, "type": 10002, params: [] });
    Text.fontSize(15);
    Text.maxLines(1);
}
function __Text__link_text_style(fontSize) {
    Text.fontSize(fontSize);
    Text.fontColor({ "id": 16777282, "type": 10001, params: [] });
    Text.fontFamily({ "id": 117440981, "type": 10002, params: [] });
    Text.maxLines(1);
}
function __TextInput__input_style() {
    TextInput.width('75%');
    TextInput.fontFamily({ "id": 117440981, "type": 10002, params: [] });
    TextInput.fontSize(15);
    TextInput.placeholderFont({ size: 15, family: { "id": 117440981, "type": 10002, params: [] } });
    TextInput.placeholderColor({ "id": 16777288, "type": 10001, params: [] });
    TextInput.caretColor({ "id": 16777283, "type": 10001, params: [] });
    TextInput.backgroundColor(Color.White);
}
function __Divider__divider_style() {
    Divider.width('90%');
    Divider.color({ "id": 16777287, "type": 10001, params: [] });
    Divider.strokeWidth(0.8);
}
class Login extends View {
    constructor(compilerAssignedUniqueChildId, parent, params) {
        super(compilerAssignedUniqueChildId, parent);
        this.updateWithValueParams(params);
    }
    updateWithValueParams(params) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id());
    }
    render() {
        Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.Start, justifyContent: FlexAlign.SpaceBetween });
        Flex.debugLine("pages/Login.ets(45:5)");
        Flex.width('100%');
        Flex.height('100%');
        Flex.backgroundColor({ "id": 16777285, "type": 10001, params: [] });
        let earlierCreatedChild_2 = this.findChildById("2");
        if (earlierCreatedChild_2 == undefined) {
            /**
             * 导航栏
             * 详见navigationBar.ets
             */
            View.create(new navigationBar_ets_1.navigationBar("2", this, { isBack: false, isClose: true, isMenu: false, isSearch: false, isMore: false }));
        }
        else {
            earlierCreatedChild_2.updateWithValueParams({
                isBack: false, isClose: true, isMenu: false, isSearch: false, isMore: false
            });
            View.create(earlierCreatedChild_2);
        }
        /**
         * 标题栏
         * 固定高度180vp
         */
        Column.create();
        Column.debugLine("pages/Login.ets(55:7)");
        /**
         * 标题栏
         * 固定高度180vp
         */
        Column.width('100%');
        /**
         * 标题栏
         * 固定高度180vp
         */
        Column.height(90);
        /**
         * 标题栏
         * 固定高度180vp
         */
        Column.padding({ top: 55 });
        Text.create({ "id": 16777246, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(56:9)");
        Text.textAlign(TextAlign.Center);
        Text.fontSize(22);
        Text.fontFamily({ "id": 117440971, "type": 10002, params: [] });
        Text.pop();
        /**
         * 标题栏
         * 固定高度180vp
         */
        Column.pop();
        /**
         * 国家+手机号码输入框
         * 固定高度400vp
         */
        Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/Login.ets(65:7)");
        /**
         * 国家+手机号码输入框
         * 固定高度400vp
         */
        Flex.width('100%');
        /**
         * 国家+手机号码输入框
         * 固定高度400vp
         */
        Flex.height(400);
        /**
         * 国家+手机号码输入框
         * 固定高度400vp
         */
        Flex.padding({ top: 20 });
        //国家/地区
        Row.create();
        Row.debugLine("pages/Login.ets(67:9)");
        //国家/地区
        Row.width('90%');
        //国家/地区
        Row.height(60);
        //国家/地区
        Row.margin({ bottom: 4, top: 4 });
        Text.create({ "id": 16777238, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(68:11)");
        __Text__text_style();
        Text.pop();
        TextInput.create({ placeholder: { "id": 16777239, "type": 10003, params: [] } });
        TextInput.debugLine("pages/Login.ets(70:11)");
        __TextInput__input_style();
        //国家/地区
        Row.pop();
        Divider.create();
        Divider.debugLine("pages/Login.ets(73:9)");
        __Divider__divider_style();
        //手机号
        Row.create();
        Row.debugLine("pages/Login.ets(75:9)");
        //手机号
        Row.width('90%');
        //手机号
        Row.height(60);
        //手机号
        Row.margin({ bottom: 4 });
        Text.create({ "id": 16777241, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(76:11)");
        __Text__text_style();
        Text.pop();
        TextInput.create({ placeholder: { "id": 16777242, "type": 10003, params: [] } });
        TextInput.debugLine("pages/Login.ets(78:11)");
        __TextInput__input_style();
        TextInput.type(InputType.Number);
        //手机号
        Row.pop();
        Divider.create();
        Divider.debugLine("pages/Login.ets(82:9)");
        __Divider__divider_style();
        //使用其他方式登陆提示
        Row.create();
        Row.debugLine("pages/Login.ets(84:9)");
        //使用其他方式登陆提示
        Row.width('90%');
        //使用其他方式登陆提示
        Row.height(50);
        //使用其他方式登陆提示
        Row.margin({ left: 2, top: 10 });
        Text.create({ "id": 16777245, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(85:11)");
        __Text__link_text_style(15);
        Text.pop();
        //使用其他方式登陆提示
        Row.pop();
        /**
         * 国家+手机号码输入框
         * 固定高度400vp
         */
        Flex.pop();
        /**
         * 底部登陆按钮和说明
         * 底部对齐方式
         */
        Flex.create({ direction: FlexDirection.Column, justifyContent: FlexAlign.End, alignItems: ItemAlign.Center });
        Flex.debugLine("pages/Login.ets(93:7)");
        /**
         * 底部登陆按钮和说明
         * 底部对齐方式
         */
        Flex.width('100%');
        /**
         * 底部登陆按钮和说明
         * 底部对齐方式
         */
        Flex.layoutWeight(1);
        //按钮提示
        Text.create({ "id": 16777237, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(95:9)");
        //按钮提示
        Text.fontColor({ "id": 16777288, "type": 10001, params: [] });
        //按钮提示
        Text.maxLines(1);
        //按钮提示
        Text.fontSize(15);
        //按钮提示
        Text.pop();
        //登陆按钮
        Button.createWithLabel({ "id": 16777236, "type": 10003, params: [] }, { type: ButtonType.Normal });
        Button.debugLine("pages/Login.ets(100:9)");
        //登陆按钮
        Button.width(170);
        //登陆按钮
        Button.height(50);
        //登陆按钮
        Button.backgroundColor({ "id": 16777283, "type": 10001, params: [] });
        //登陆按钮
        Button.fontSize(18);
        //登陆按钮
        Button.fontColor({ "id": 16777290, "type": 10001, params: [] });
        //登陆按钮
        Button.borderRadius(8);
        //登陆按钮
        Button.margin({ top: 30 });
        //登陆按钮
        Button.onClick(() => {
            router.push({ uri: 'pages/Main' });
        });
        //登陆按钮
        Button.pop();
        //底部说明页脚
        Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.End, justifyContent: FlexAlign.Center });
        Flex.debugLine("pages/Login.ets(112:9)");
        //底部说明页脚
        Flex.width('100%');
        //底部说明页脚
        Flex.padding({ bottom: 30, top: 60 });
        Text.create({ "id": 16777243, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(113:11)");
        __Text__link_text_style(12);
        Text.margin({ right: 5 });
        Text.pop();
        Text.create('|');
        Text.debugLine("pages/Login.ets(116:11)");
        __Text__link_text_style(12);
        Text.margin({ right: 5 });
        Text.pop();
        Text.create({ "id": 16777240, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(119:11)");
        __Text__link_text_style(12);
        Text.margin({ right: 5 });
        Text.pop();
        Text.create('|');
        Text.debugLine("pages/Login.ets(122:11)");
        __Text__link_text_style(12);
        Text.margin({ right: 5 });
        Text.pop();
        Text.create({ "id": 16777244, "type": 10003, params: [] });
        Text.debugLine("pages/Login.ets(125:11)");
        __Text__link_text_style(12);
        Text.pop();
        //底部说明页脚
        Flex.pop();
        /**
         * 底部登陆按钮和说明
         * 底部对齐方式
         */
        Flex.pop();
        Flex.pop();
    }
}
loadDocument(new Login("1", undefined, {}));

})();

/******/ })()
;