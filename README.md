# HMWeChat

#### 介绍
HMWeChat（HarmonyOS WeChat）是基于鸿蒙3.0的ArkUI开发的仿微信界面，使用的是eTS开发范式，基于鸿蒙SDK3.0（API Version 7）。

注：该只有前端UI，无后端逻辑。

#### 数据模型说明
UI界面的显示数据通过数据模型进行定义更新，所有模型均位于ets/default/model下，后端逻辑可参考模型接口对接。

data包中为数据结构定义

Model文件为模型接口

#### 软件截图（模拟器）

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/1_.gif)

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/2_.gif)

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/3_.gif)

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/4_.gif)

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/5_.gif)

![Image text](https://gitee.com/yiyefangzhou24/hmwechat/raw/master/doc/6_.gif)


#### 参与贡献

本项目为yiyefangzhou24独立开发，如果您在使用这个项目的过程中遇到任何问题，或者您对我的项目有任何意见或者建议，再或者有不错的想法欢迎与我交流，邮件：**yiyefangzhou24@qq.com**。